<%-- 
    Document   : detalles
    Created on : 02-may-2018, 16:24:25
    Author     : gabymedinacolegio
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="detalles" class="sv.edu.udb.beans.Documento" scope="page"/>
<jsp:useBean id="ejemplares" class="sv.edu.udb.beans.Ejemplar" scope="page"/>
<jsp:setProperty name="detalles" property="id" param="id" />
<jsp:setProperty name="ejemplares" property="idDocumento" param="id" />
<%
    detalles.getDatos();
    
    if(detalles.getTitulo() == null){
        response.sendRedirect("index.jsp");
    } else {
        ejemplares.getLista();
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- <link rel="icon" href="../../favicon.ico"> -->

		<title><fmt:message key="titulo.sistema"/></title>

		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		<link href="css/dashboard.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
        <body>

		<jsp:include page="navBar.jsp" />

		<div class="container">
                    <div class="row">

			<div class="col-sm-12 col-md-12 col-xs-12">
                <h1 class="page-header"><fmt:message key="resultado.detalle"/></h1>
                    <div class="form-horizontal">
                        <c:if test="${ not empty detalles.isbn}">
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><fmt:message key="documento.isbn"/>:</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static">${detalles.isbn}</p>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${ not empty detalles.titulo}">
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><fmt:message key="documento.tituloMat"/>:</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static">${detalles.titulo}</p>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${ not empty detalles.autor}">
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><fmt:message key="documento.autores"/>:</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static">${detalles.autor}</p>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${ not empty detalles.editorial}">
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><fmt:message key="documento.editorial"/>:</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static">${detalles.editorial}</p>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${ not empty detalles.edicion}">
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><fmt:message key="documento.edicion"/>:</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static">${detalles.edicion}</p>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${ not empty detalles.resumen}">
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><fmt:message key="documento.resumen"/>:</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static">${detalles.resumen}</p>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${ not empty detalles.notas}">
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><fmt:message key="documento.notas"/>:</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static">${detalles.notas}</p>
                                </div>
                            </div>
                        </c:if>
                    </div>
                    <div class="divide-10"></div>
                    <h3><fmt:message key="documento.ejemplares"/></h3>
                    <div class="col-md-4">
                        <table id="tablaPrestamos" name="tablaPrestamos" class="table table-striped table-responsive" >
                            <thead>
                                <tr>
                                    <th><fmt:message key="documento.id"/></th>
                                    <th><fmt:message key="documento.disponibilidad"/></th>
                                </tr>
                            </thead>
                            <tbody id="cuerpoTabla" name="cuerpoTabla">
                                <c:forEach items="${ejemplares.lista}" var="registro">
                                    <td>${registro.id}</td>
                                    <c:choose>
                                        <c:when test="${registro.disponibilidad}">
                                            <td><i class="fa fa-check text-success"></i> <fmt:message key="disp.si"/></td>
                                        </c:when>
                                        <c:when test="${!registro.disponibilidad}">
                                            <td><i class="fa fa-times text-danger"></i> <fmt:message key="disp.no"/></td>
                                        </c:when>
                                    </c:choose>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/ie10-viewport-bug-workaround.js"></script>
        
	</body>
        
</html>