<%-- 
    Document   : infoDocumentos
    Created on : 04-24-2018, 04:07:33 PM
    Author     : José Fernando Flores Santamaría - FS150192
--%>
<%@page session="true"%>
<%
    HttpSession sesionUsuario = request.getSession();
    
    if(sesionUsuario.getAttribute("id") == null){
        response.sendRedirect("login.jsp?error=sesion");
    } else {
        if(!sesionUsuario.getAttribute("tipo").toString().equals("1")){
            response.sendRedirect("estado.jsp");
        }
    }
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="listaCategoria" class="sv.edu.udb.beans.Categoria" scope="page"/>
<jsp:useBean id="infod" class="sv.edu.udb.beans.Documento" scope="session"/>
<jsp:setProperty name="infod" property="id" param="id" />
<% 
    listaCategoria.getLista();
    if(request.getParameter("id") != null){
        infod.getDatos();
    }
%>
<c:if test="${empty param}">
    <c:remove var="infod" scope="session" />
</c:if>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- <link rel="icon" href="../../favicon.ico"> -->

		<title><fmt:message key="titulo.sistema"/></title>

		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		<link href="css/dashboard.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>

		<jsp:include page="navBarAdmin.jsp" />

		<div class="container-fluid">
			<div class="row">

                <jsp:include page="sideBar.jsp" />

				<div class="col-sm-9 col-md-10 col-md-offset-2 col-sm-offset-3 col-xs-12 main">
					<h1 class="page-header"><fmt:message key="documento.titulo"/></h1>
                                        
                                        <c:if test="${not empty param.done}">
                                            <div class="alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <strong>
                                                    <c:choose>
                                                        <c:when test="${param.done == 'insertar'}" >
                                                            <strong><fmt:message key="documento.notif2"/></strong>                                 
                                                        </c:when>
                                                    </c:choose>
                                                </strong>
                                            </div>
                                        </c:if>
                                        
                                        <c:if test="${param.done == 'insertar'}">
                                            <c:remove var="infod" scope="session" />
                                        </c:if>


					<!-- <div class="alert alert-danger alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<strong>¡Error!</strong> Alerta de error.
					</div> -->

					<div class="col-md-12">
						<h3>Datos: </h3>
                                                <c:choose>
                                                    <c:when test="${empty param || param.done == 'insertar'}" >
                                                        <form action="manto/mantoDocumento.jsp?action=insertar" method="POST">
                                                    </c:when>
                                                    <c:when test="${param.action == 'editar' || param.done == 'editar'}">
                                                        <form action="manto/mantoDocumento.jsp?action=editar" method="POST">
                                                    </c:when>
                                                </c:choose>                                                
							<div class="form-group col-md-12">
								<label for="id"><fmt:message key="documento.id"/>: 
									<a data-toggle="tooltip" data-placement="right" title="<fmt:message key="ayuda.id"/>">
										<i class="fa fa-question-circle"></i>
									</a>
								</label>
								<input type="text" class="form-control" id="id" name="id" required="true" readonly="true" value="<c:out value="${infod.id}" />" />
							</div>
							<div class="form-group col-md-6">
								<label for="titulo"><fmt:message key="documento.tituloMat"/>: </label>
								<input type="text" class="form-control" id="titulo" name="titulo" required="true" value="<c:out value="${infod.titulo}" />" />
							</div>
							<div class="form-group col-md-6">
								<label for="isbn"><fmt:message key="documento.isbn"/>: </label>
								<input type="text" class="form-control" id="isbn" name="isbn" value="<c:out value="${infod.isbn}" />"/>
							</div>
							<div class="form-group col-md-12">
								<label for="autor"><fmt:message key="documento.autores"/>: 
									<a data-toggle="tooltip" data-placement="right" title="<fmt:message key="ayuda.autores"/>">
										<i class="fa fa-question-circle"></i>
									</a>
								</label>
								<textarea class="form-control" id="autor" name="autor" rows="3" required="true" value="<c:out value="${infod.autor}" />"></textarea>
							</div>            
							<div class="form-group col-md-6">
								<label for="edicion"><fmt:message key="documento.edicion"/>: </label>
								<input type="number" class="form-control" id="edicion" name="edicion" min="1" value="<c:out value="${infod.edicion}" />" />
							</div>    
							<div class="form-group col-md-6">
								<label for="editorial"><fmt:message key="documento.editorial"/>: </label>
								<input type="text" class="form-control" id="editorial" name="editorial" value="<c:out value="${infod.editorial}" />" />
							</div>            
							<div class="form-group col-md-6">
								<label for="recurso"><fmt:message key="documento.digital"/>: </label>
								<input type="text" class="form-control" id="recurso" name="recurso" value="<c:out value="${infod.recurso}" />" />
							</div>            
							<div class="form-group col-md-6">
								<label for="ubicacion"><fmt:message key="documento.ubicacion"/>: </label>
								<input type="text" class="form-control" id="ubicacion" name="ubicacion" required="true" value="<c:out value="${infod.ubicacion}" />" />
							</div>            
							<div class="form-group col-md-6">
								<label for="contenido"><fmt:message key="documento.contenido"/>: </label>
								<textarea class="form-control" id="contenido" name="contenido" rows="2" value="<c:out value="${infod.contenido}" />"></textarea>
							</div> 
							<div class="form-group col-md-6">
								<label for="temas"><fmt:message key="documento.temas"/>: </label>
								<textarea class="form-control" id="temas" name="temas" rows="2" value="<c:out value="${infod.temas}" />"></textarea>
							</div> 
							<div class="form-group col-md-6">
								<label for="descripcion"><fmt:message key="documento.descripcion"/>: </label>
								<textarea class="form-control" id="descripcion" name="descripcion" rows="2" value="<c:out value="${infod.descripcion}" />"></textarea>
							</div> 
							<div class="form-group col-md-6">
								<label for="resumen"><fmt:message key="documento.resumen"/>: </label>
								<textarea class="form-control" id="resumen" name="resumen" rows="2" value="<c:out value="${infod.resumen}" />"></textarea>
							</div> 
							<div class="form-group col-md-6">
								<label for="notas"><fmt:message key="documento.notas"/>: </label>
								<textarea class="form-control" id="notas" name="notas" rows="2" value="<c:out value="${infod.notas}" />"></textarea>
							</div> 
							<div class="form-group col-md-3">
								<label for="cantidad"><fmt:message key="documento.ejemCant"/>: </label>
								<input type="number" class="form-control" id="cantidad" name="cantidad" required="true" min="1" />
							</div>
							<div class="form-group col-md-3">
								<label for="categoria"><fmt:message key="documento.categoria"/>: </label>
								<select class="form-control" id="categoria" name="categoria">
                                                                        <c:forEach items="${listaCategoria.lista}" var="registro">
                                                                            <option value="${registro.id}">${registro.categoria}</option>
                                                                        </c:forEach>
								</select>
							</div>
							
							<div class="form-group col-md-12">
								<input type="submit" class="btn btn-primary pull-right" id="btnGuardar" name="btnGuardar" value="<fmt:message key="boton.guardar"/>" />
							</div>
						</form>
					</div>
									
				</div>
			</div>
		</div>                
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/ie10-viewport-bug-workaround.js"></script>
                <script src="js/util.js"></script>
                <script type="text/javascript">
                    var idCat;
                    var isbn;
                    var edicion;
                    var resumen;
                    var recurso;
                    $(document).ready(function() {
                        $("#categoria").change(function() {
                            idCat = $("#categoria").val();

                            $.ajax({
                                url: "DatosCategoria",
                                type: "POST",
                                data: { 
                                    id : idCat
                                },
                                dataType: "json",
                                encode: "true"
                            })
                            .done(function(datos) {
                                isbn = datos.isbn;
                                resumen = datos.resumen;
                                edicion = datos.edicion;
                                recurso = datos.recurso;
                                
                                if(!isbn)
                                    $("#isbn").prop('disabled', true);
                                else
                                    $("#isbn").prop('disabled', false);
                                if(!resumen)
                                    $("#resumen").prop('disabled', true);
                                else
                                    $("#resumen").prop('disabled', false);
                                if(!edicion)
                                    $("#edicion").prop('disabled', true);
                                else
                                    $("#edicion").prop('disabled', false);
                                if(!recurso)
                                    $("#recurso").prop('disabled', true);
                                else
                                    $("#recurso").prop('disabled', false);
                            })
                            .fail(function(){
                                alert("Error");
                            });
                        });
                        
                        $( window ).load(function() {
                            idCat = $("#categoria").val();

                            $.ajax({
                                url: "DatosCategoria",
                                type: "POST",
                                data: { 
                                    id : idCat
                                },
                                dataType: "json",
                                encode: "true"
                            })
                            .done(function(datos) {
                                isbn = datos.isbn;
                                resumen = datos.resumen;
                                edicion = datos.edicion;
                                recurso = datos.recurso;
                                
                                if(!isbn)
                                    $("#isbn").prop('disabled', true);
                                else
                                    $("#isbn").prop('disabled', false);
                                if(!resumen)
                                    $("#resumen").prop('disabled', true);
                                else
                                    $("#resumen").prop('disabled', false);
                                if(!edicion)
                                    $("#edicion").prop('disabled', true);
                                else
                                    $("#edicion").prop('disabled', false);
                                if(!recurso)
                                    $("#recurso").prop('disabled', true);
                                else
                                    $("#recurso").prop('disabled', false);
                            })
                            .fail(function(){
                                alert("Error");
                            });
                        });
                        
                    });
                </script>
	</body>
</html>