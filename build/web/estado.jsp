<%-- 
    Document   : estado
    Created on : 01-may-2018, 14:20:49
    Author     : gabymedinacolegio
--%>
<%@page import="java.text.DecimalFormat"%>
<%@page session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="datos" class="sv.edu.udb.beans.Usuario" scope="page"/>
<%
    HttpSession sesionUsuario = request.getSession();
    DecimalFormat df = new DecimalFormat("#0.00");
    
    if(sesionUsuario.getAttribute("id") == null){
        response.sendRedirect("login.jsp?error=sesion");
    } else {
        datos.setId(sesionUsuario.getAttribute("id").toString());
        datos.getDatos();
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- <link rel="icon" href="../../favicon.ico"> -->

		<title><fmt:message key="titulo.sistema"/></title>

		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		<link href="css/dashboard.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>

		<jsp:include page="navBar.jsp" />

		<div class="container">
			<div class="row">

				<div class="col-sm-12 col-md-12 col-xs-12">
					<h1 class="page-header"><fmt:message key="titulo.estado"/></h1>
					
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <fmt:message key="titulo.estado"/>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="col-md-8 col-xs-12">
                                        <form>
                                            <div class="form-group col-md-3 col-xs-12 col-sm-12">
                                                <label for="id"><fmt:message key="usuario.id"/>: </label>
                                                <input type="text" class="form-control" id="id" name="id" readonly="true" value="${datos.id}"/>
                                            </div>
                                            <div class="clearfix visible-xs"></div>
                                            <div class="form-group col-md-12">
                                                <label for="nombre"><fmt:message key="usuario.nombre"/>: </label>
                                                <input type="text" class="form-control" id="nombre" name="nombre" readonly="true" value="${datos.nombres} ${datos.apellidos}"/>
                                            </div>
                                            <div class="form-group col-md-5">
                                                <label for="rol"><fmt:message key="usuario.tipo"/>: </label>
                                                <select class="form-control" id="rol" name="rol" readonly="true" disabled="true">
                                                    <option><fmt:message key="tipo.admin"/></option>
                                                    <option><fmt:message key="tipo.maestro"/></option>
                                                    <option><fmt:message key="tipo.alumno"/></option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-5 col-md-offset-2 ">
                                                <label for="prestados"><fmt:message key="prestamo.prestados"/>: </label>
                                                <input type="text" class="form-control" id="prestados" name="prestados" readonly="true" value="${datos.prestados}"/>
                                            </div>            
                                        </form>
                                    </div> 
                                    <div class="col-md-3 col-xs-12 col-md-offset-1">
                                        <div class="divide-60 hidden-sm hidden-xs"></div>
                                        <div class="panel panel-default">
                                            <div id="cont-mora" name="cont-mora" class="panel-body panel-mora-success">
                                                <h2 class="text-center"><fmt:message key="usuario.mora"/></h2>
                                                <h2 class="text-center" id="totalMora">$<%= df.format(datos.getMora()) %></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                
                    </div>
                    
                    <div class="divide-10"></div>
                    <h3><fmt:message key="prestamo.prestamosUsuario"/></h3>
                    <div class="col-md-12">
                        <table id="tablaPrestamos" name="tablaPrestamos" class="table table-striped table-responsive" >
                            <thead>
                                <tr>
                                    <th><fmt:message key="prestamo.ejemplar"/></th>
                                    <th><fmt:message key="prestamo.tituloMat"/></th>
                                    <th><fmt:message key="prestamo.tipo"/></th>
                                    <th><fmt:message key="prestamo.fi"/></th>
                                    <th><fmt:message key="prestamo.ff"/></th>
                                </tr>
                            </thead>
                            <tbody id="cuerpoTabla" name="cuerpoTabla">
                                <tr><td colspan='7' class='text-center'><fmt:message key="prestamo.notif2"/></td></tr>
                            </tbody>
                        </table>
                    </div>
				</div>
			</div>
		</div>

		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/ie10-viewport-bug-workaround.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("select#rol")[0].selectedIndex = ${datos.tipo - 1};
                
                $.ajax({
                    url: "ListaPrestamos",
                    type: "POST",
                    data: { 
                        id : '${datos.id}',
                        tabla : 'estado',
                        lang : '<fmt:message key="util.idioma"/>'
                    },
                    dataType: "html",
                    encode: "true"
                })
                .done(function(datos) {
                    if(datos) {
                        $("#cuerpoTabla").html(datos);
                    } else {
                        $("#cuerpoTabla").html("<tr><td colspan='7' class='text-center'> <fmt:message key="prestamo.notif2"/></td></tr>");
                    }                                         
                })
                .fail(function(){
                    alert("Error Tabla");
                });
            });
        </script>
	</body>
</html>