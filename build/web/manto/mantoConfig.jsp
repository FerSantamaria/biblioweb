<%-- 
    Document   : mantoConfig
    Created on : 05-04-2018, 10:39:17 AM
    Author     : José Fernando Flores Santamaría - FS150192
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="ajustes" class="sv.edu.udb.beans.Configuracion" scope="request"/>
<jsp:setProperty name="ajustes" property="id" param="rol" />
<jsp:setProperty name="ajustes" property="cant" param="ejemplares" />
<jsp:setProperty name="ajustes" property="dias" param="dias" />

<% 
    String act = request.getParameter("action");
    
    switch(act){
        case "editar":
            if(ajustes.getEdicion()){
                response.sendRedirect("../configuracion.jsp?done=editarAjustes");
            } else {
                response.sendRedirect("../configuracion.jsp");
            }
            break;
    }
%>