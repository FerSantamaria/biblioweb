<%-- 
    Document   : ingresarUsuario
    Created on : 04-25-2018, 10:55:57 PM
    Author     : José Fernando Flores Santamaría - FS150192
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="info" class="sv.edu.udb.beans.Usuario" scope="session"/>
<jsp:setProperty name="info" property="id" param="id" />
<jsp:setProperty name="info" property="nombres" param="nombres" />
<jsp:setProperty name="info" property="apellidos" param="apellidos" />
<jsp:setProperty name="info" property="tipo" param="tipo" />

<% 
    String act = request.getParameter("action");
    
    switch(act){
        case "insertar":
            info.getInsertar();
            request.setAttribute("done","insertar");
            response.sendRedirect("../infoUsuario.jsp?done=insertar");
            break;
        case "editar":
            info.getEdicion();
            request.setAttribute("done","editar");
            response.sendRedirect("../infoUsuario.jsp?done=editar");
            break;
        case "eliminar":
            info.getEliminacion();
            request.setAttribute("done","eliminar");
            if(info.isError()){
                response.sendRedirect("../usuarios.jsp?done=eliminar&error=true");
            } else {
                response.sendRedirect("../usuarios.jsp?done=eliminar");
            }
            break;
    }
%>