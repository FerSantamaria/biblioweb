<%-- 
    Document   : mantoCategoria
    Created on : may 5, 2018, 9:26:24 a.m.
    Author     : malfa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="infoc" class="sv.edu.udb.beans.Categoria" scope="session"/>
<jsp:setProperty name="infoc" property="id" param="id" />
<jsp:setProperty name="infoc" property="categoria" param="categoria" />
<jsp:setProperty name="infoc" property="isbn" param="isbn" />
<jsp:setProperty name="infoc" property="edicion" param="edicion" />
<jsp:setProperty name="infoc" property="resumen" param="resumen" />
<jsp:setProperty name="infoc" property="recurso" param="recurso" />

<% 
    String act = request.getParameter("action");
    
    switch(act){
        case "insertar":
            infoc.getInsertar();
            request.setAttribute("done","insertar");
            response.sendRedirect("../infoCategoria.jsp?done=insertar");
            break;
            
        case "editar":
            infoc.getEditar();
            request.setAttribute("done","editar");
            response.sendRedirect("../infoCategoria.jsp?done=editar");
            break;
            
        case "eliminar":
            infoc.getEliminar();
            request.setAttribute("done","eliminar");
            if(infoc.isError()){
                response.sendRedirect("../categorias.jsp?done=eliminar&error=true");
            } else {
                response.sendRedirect("../categorias.jsp?done=eliminar");
            }
            break;
    }
%>
