<%-- 
    Document   : infoUsuario
    Created on : 04-24-2018, 04:04:28 PM
    Author     : Jos� Fernando Flores Santamar�a - FS150192
--%>
<%@page session="true"%>
<%
    HttpSession sesionUsuario = request.getSession();
    
    if(sesionUsuario.getAttribute("id") == null){
        response.sendRedirect("login.jsp?error=sesion");
    } else {
        if(!sesionUsuario.getAttribute("tipo").toString().equals("1")){
            response.sendRedirect("estado.jsp");
        }
    }
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="info" class="sv.edu.udb.beans.Usuario" scope="session"/>
<jsp:setProperty name="info" property="id" param="id" />
<% 
    if(request.getParameter("id") != null){
        info.getDatos();
    }
%>

<c:if test="${empty param}">
    <c:remove var="info" scope="session" />
</c:if>

<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- <link rel="icon" href="../../favicon.ico"> -->

		<title><fmt:message key="titulo.sistema"/></title>

		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		<link href="css/dashboard.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>

		<jsp:include page="navBarAdmin.jsp" />

		<div class="container-fluid">
			<div class="row">

                <jsp:include page="sideBar.jsp" />

				<div class="col-sm-9 col-md-10 col-md-offset-2 col-sm-offset-3 col-xs-12 main">
					<h1 class="page-header"><fmt:message key="usuario.titulo"/></h1>
					
                    
                    <c:if test="${not empty param.done}">
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>
                                <c:choose>
                                    <c:when test="${param.done == 'insertar'}" >
                                        <fmt:message key="usuario.registrar"/> <fmt:message key="usuario.id"/>: ${info.id}, <fmt:message key="usuario.contra"/>: ${info.contrasena}                                      
                                    </c:when>
                                    <c:when test="${param.done == 'editar'}" >
                                        <fmt:message key="usuario.editar"/>
                                    </c:when>
                                </c:choose>
                            </strong>
                        </div>
                    </c:if>
                    
                        <!--<div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>�Error!</strong> 
                        </div>-->
                    
                    <c:if test="${param.done == 'insertar'}">
                        <c:remove var="info" scope="session" />
                    </c:if>
					                    
					<div class="col-md-12">
						<h3><fmt:message key="usuario.datos"/>: </h3>
                        
                        <c:choose>
                            <c:when test="${empty param || param.done == 'insertar'}" >
                                <form action="manto/mantoUsuario.jsp?action=insertar" method="POST">
                            </c:when>
                            <c:when test="${param.action == 'editar' || param.done == 'editar'}">
                                <form action="manto/mantoUsuario.jsp?action=editar" method="POST">
                            </c:when>
                        </c:choose>
                            <div class="form-group col-md-12">
								<label for="id"><fmt:message key="usuario.id"/>: 
									<a data-toggle="tooltip" data-placement="right" title="<fmt:message key="ayuda.id"/>">
										<i class="fa fa-question-circle"></i>
									</a>
								</label>
                                <input type="text" class="form-control" id="id" name="id" required="true" readonly="true" value="<c:out value="${info.id}" />"/>
							</div>
							<div class="form-group col-md-6">
								<label for="nombres"><fmt:message key="usuario.nombres"/>: </label>
                                <input type="text" class="form-control" id="nombres" name="nombres" required="true" value="<c:out value="${info.nombres}" />"/>
							</div>
							<div class="form-group col-md-6">
								<label for="apellidos"><fmt:message key="usuario.apellidos"/>: </label>
                                <input type="text" class="form-control" id="apellidos" name="apellidos" required="true" value="<c:out value="${info.apellidos}" />"/>
							</div>
							<!--<div class="form-group col-md-6">
								<label for="correo">Correo Electr�nico: </label>
								<input type="email" class="form-control" id="correo" name="correo" />
							</div>-->
							<div class="form-group col-md-6">
								<label for="tipo"><fmt:message key="usuario.tipo"/> </label>
								<select class="form-control" id="tipo" name="tipo">
									<option value="1"><fmt:message key="tipo.admin"/></option>
									<option value="2"><fmt:message key="tipo.maestro"/></option>
									<option value="3" selected="true"><fmt:message key="tipo.alumno"/></option>
								</select>
							</div>
                            <div class="clearfix">
                            </div>
							<div class="form-group col-md-12">
								<input type="submit" class="btn btn-primary pull-right" id="btnGuardar" name="btnGuardar" value="<fmt:message key="boton.guardar"/>" />
							</div>
						</form>
					</div>
									
				</div>
			</div>
		</div>

		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/ie10-viewport-bug-workaround.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
					$('[data-toggle="tooltip"]').tooltip();
					// $('[data-toggle="popover"]').popover();
                    <c:if test="${info.tipo >= 1}">
                            $("select#tipo")[0].selectedIndex = <c:out value='${info.tipo - 1}' />;
                    </c:if>                    
			});
		</script>   
	</body>
</html>

