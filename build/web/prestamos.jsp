<%-- 
    Document   : prestamos
    Created on : 04-24-2018, 04:48:58 PM
    Author     : José Fernando Flores Santamaría - FS150192
--%>
<%@page session="true"%>
<%
    HttpSession sesionUsuario = request.getSession();
    
    if(sesionUsuario.getAttribute("id") == null){
        response.sendRedirect("login.jsp?error=sesion");
    } else {
        if(!sesionUsuario.getAttribute("tipo").toString().equals("1")){
            response.sendRedirect("estado.jsp");
        }
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- <link rel="icon" href="../../favicon.ico"> -->

		<title><fmt:message key="titulo.sistema"/></title>

		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		<link href="css/dashboard.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>

		<jsp:include page="navBarAdmin.jsp" />

		<div class="container-fluid">
			<div class="row">

                <jsp:include page="sideBar.jsp" />

				<div class="col-sm-9 col-md-10 col-md-offset-2 col-sm-offset-3 col-xs-12 main">
					<h1 class="page-header"><fmt:message key="prestamo.titulo"/></h1>
					
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <fmt:message key="prestamo.datosUsuario"/>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="col-md-8 col-xs-12">
                                        <form>
                                            <div class="form-group col-md-3 col-xs-6">
                                                <label for="id"><fmt:message key="usuario.id"/>: </label>
                                                <input type="text" class="form-control" id="id" name="id" required="true" />
                                            </div>
                                            <div class="form-group col-md-9 col-xs-6">
                                                <div class="divide-25"></div>
                                                <input type="button" class="btn btn-primary" id="btnBuscarUsuario" name="btnBuscarUsuario" value="<fmt:message key="boton.buscar"/>" />
                                                <input type="button" class="btn btn-primary" id="btnLimpiarUsuario" name="btnLimpiarUsuario" value="<fmt:message key="boton.limpiar"/>" />
                                            </div> 
                                            <div class="clearfix visible-xs"></div>
                                            <div class="form-group col-md-12">
                                                <label for="nombre"><fmt:message key="usuario.nombre"/>: </label>
                                                <input type="text" class="form-control" id="nombre" name="nombre" readonly="true"/>
                                            </div>
                                            <div class="form-group col-md-5">
                                                <label for="rol"><fmt:message key="usuario.tipo"/>: </label>
                                                <select class="form-control" id="rol" name="rol" readonly="true" disabled="true">
                                                    <option><fmt:message key="tipo.admin"/></option>
                                                    <option><fmt:message key="tipo.maestro"/></option>
                                                    <option><fmt:message key="usuario.alumno"/></option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-5 col-md-offset-2 ">
                                                <label for="prestados"><fmt:message key="prestamo.prestados"/>: </label>
                                                <input type="text" class="form-control" id="prestados" name="prestados" readonly="true" />
                                            </div>            
                                        </form>
                                    </div> 
                                    <div class="col-md-3 col-xs-12 col-md-offset-1">
                                        <div class="divide-60 hidden-sm hidden-xs"></div>
                                        <div class="panel panel-default">
                                            <div id="cont-mora" name="cont-mora" class="panel-body panel-mora-success">
                                                <h2 class="text-center"><fmt:message key="usuario.mora"/></h2>
                                                <h2 class="text-center" id="totalMora"></h2>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <fmt:message key="prestamo.datosMaterial"/>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <div class="col-md-8 col-xs-12">
                                        <form>
                                            <div class="form-group col-md-3 col-xs-6">
                                                <label for="idMaterial"><fmt:message key="documento.id"/>: </label>
                                                <input type="text" class="form-control" id="idMaterial" name="idMaterial" required="true" />
                                            </div>
                                            <div class="form-group col-md-9 col-xs-6">
                                                <div class="divide-25"></div>
                                                <input type="button" class="btn btn-primary" id="btnBuscarMaterial" name="btnBuscarMaterial" value="<fmt:message key="boton.buscar"/>" />
                                                <input type="button" class="btn btn-primary" id="btnLimpiarMaterial" name="btnLimpiarMaterial" value="<fmt:message key="boton.limpiar"/>" />
                                            </div> 
                                            <div class="clearfix visible-xs"></div>
                                            <div class="form-group col-md-12">
                                                <label for="titulo"><fmt:message key="documento.tituloMat"/>: </label>
                                                <input type="text" class="form-control" id="titulo" name="titulo" readonly="true" />
                                            </div>
                                            <div class="form-group col-md-5">
                                                <label for="disponibilidad"><fmt:message key="documento.disponibilidad"/>: </label>
                                                <input type="text" class="form-control" id="disponibilidad" name="disponibilidad" readonly="true" />
                                            </div>
                                            <div class="form-group col-md-5 col-xs-12 col-md-offset-2">
                                                <div class="divide-25"></div>
                                                <input type="button" class="btn btn-primary pull-right" id="btnVerMas" name="btnVerMas" data-documento="0" value="<fmt:message key="boton.verMas"/>" disabled="true"/>
                                            </div>            
                                        </form>
                                    </div> 
                                    <div class="col-md-3 col-xs-12 col-md-offset-1">
                                        <div class="divide-80 hidden-sm hidden-xs"></div>
                                        <div class="col-md-3">
                                            <input type="button" class="btn btn-primary btn-lg" id="btnPrestar" name="btnPrestar" value="<fmt:message key="boton.prestar"/>" disabled="true"/>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="divide-10"></div>
                    <h3><fmt:message key="prestamo.prestamosUsuario"/></h3>
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="tablaPrestamos" name="tablaPrestamos" class="table table-striped" >
                                <thead>
                                    <tr>
                                        <th class="hidden-xs"><fmt:message key="prestamo.id"/></th>
                                        <th class="hidden-xs"><fmt:message key="prestamo.ejemplar"/></th>
                                        <th><fmt:message key="prestamo.tituloMat"/></th>
                                        <th><fmt:message key="prestamo.tipo"/></th>
                                        <th><fmt:message key="prestamo.fi"/></th>
                                        <th><fmt:message key="prestamo.ff"/></th>
                                        <th><fmt:message key="prestamo.opcion"/></th>
                                    </tr>
                                </thead>
                                <tbody id="cuerpoTabla" name="cuerpoTabla">
                                    <tr><td colspan='7' class='text-center'><fmt:message key="prestamo.tablaVacia"/></td></tr>
                                </tbody>
                            </table>
                        </div>                        
                    </div>
                    <div id="modalMensaje" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="titModal"></h4>
                                </div>
                                <strong>
                                    <div class="modal-body" id="contModal"></div>
                                </strong>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>

		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/ie10-viewport-bug-workaround.js"></script>
        <jsp:include page="utilPrestamos.jsp" />
	</body>
</html>
