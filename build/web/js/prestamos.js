var idUser, moraUser, maxUser, prestUser, diasUSer;
var idMaterial, dispMat;

$(document).ready(function() {
    $("#btnBuscarUsuario").click(function(event) {
        idUser = $("#id").val().trim();

        if(idUser){
            $("#id").prop("readonly", true);
            $(this).prop("disabled", true);

            $.ajax({
                url: "DatosUsuario",
                type: "POST",
                data: { 
                    id : idUser
                },
                dataType: "json",
                encode: "true"
            })
            .done(function(datos) {
                if(datos.nombres){
                    $("#nombre").val(datos.nombres + " " + datos.apellidos);
                    $("#rol").prop("selectedIndex", datos.tipo - 1);
                    $("#prestados").val(datos.prestados);
                    $("#totalMora").html("$"+datos.mora);
                    prestUser = parseInt(datos.prestados);
                    moraUser = parseFloat(datos.mora);
                    maxUser = parseInt(datos.maximo);
                    diasUSer = parseInt(datos.dias);
                    
                    if (moraUser > 0) {
                        $("#cont-mora").removeClass("panel-mora-succes");
                        $("#cont-mora").addClass("panel-mora-danger");
                    } else {
                        $("#cont-mora").removeClass("panel-mora-danger");
                        $("#cont-mora").addClass("panel-mora-success");
                    }

                    llenarTabla();
                    habilitarPrestamo();
                } else {
                    limpiarUsuario();
                    mensaje("Error", "Usuario no encontrado");
                }                           
            })
            .fail(function(){
                $("#id").prop("readonly", false);
                alert("Error");
            }); 
        }                   
    });

    $("#btnBuscarMaterial").click(function(event) {
        $("#idMaterial").prop("readonly", true);
        idMaterial = $("#idMaterial").val();

        if(idMaterial){
            $(this).prop("disabled", true);
            $.ajax({
                url: "DatosMaterial",
                type: "POST",
                data: { 
                    id : idMaterial
                },
                dataType: "json",
                encode: "true"
            })
            .done(function(datos) {
                if(datos.titulo){
                    $("#titulo").val(datos.titulo);
                    $("#btnVerMas").prop("disabled", false);
                    $("#btnVerMas").attr("data-documento", datos.documento);

                    if(datos.disponibilidad){
                        $("#disponibilidad").val("SI");
                        dispMat = true;
                    } else {
                        $("#disponibilidad").val("NO");
                        dispMat = false;
                    }
                    habilitarPrestamo();
                    
                } else {
                    limpiarMaterial();
                    mensaje("Error", "Material no encontrado");
                    $('#modalMensaje').modal('show');
                }                                                   
            })
            .fail(function(){
                $("#idMaterial").prop("readonly", false);
                alert("Error");
            });
        }
    });
    
    $("#btnPrestar").click(function (){
        $(this).prop("disabled", true);
        $.ajax({
            url: "PrestarMaterial",
            type: "POST",
            data: { 
                material : idMaterial,
                usuario : idUser,
                dias : diasUSer
            },
            dataType: "json",
            encode: "true"
        })
        .done(function(datos) {
            $("#btnLimpiarMaterial").click();
            $("#btnBuscarUsuario").click();
            mensaje("Préstamo", "Prestamo realizado con éxito");
        })
        .fail(function(){
            alert("Error");
        });
    });
});

$(document).on("click",".devolver", function(){
    devolver($(this).data("idprestamo"), $(this).data("idmaterial"));
});

function mensaje(titulo, mensaje){
    $('#titModal').html(titulo);
    $('#contModal').html(mensaje);
    $('#modalMensaje').modal('show');
}
function llenarTabla(){
    $.ajax({
        url: "ListaPrestamos",
        type: "POST",
        data: { 
            id : idUser
        },
        dataType: "html",
        encode: "true"
    })
    .done(function(datos) {
        if(datos) {
            $("#cuerpoTabla").html(datos);
        } else {
            $("#cuerpoTabla").html("<tr><td colspan='7' class='text-center'> No hay préstamos activos</td></tr>");
        }                                         
    })
    .fail(function(){
        alert("Error Tabla");
    });
}

function limpiarUsuario(){
    $("#id").val("");
    $("#id").prop("readonly", false);
    $("#btnBuscarUsuario").prop("disabled", false);

    $("#nombre").val("");
    $("#rol").prop("selectedIndex", 0);
    $("#prestados").val("");
    $("#totalMora").html("");
    $("#btnPrestar").prop("disabled", true);
    
    if (moraUser > 0) {
        $("#cont-mora").removeClass("panel-mora-danger");
        $("#cont-mora").addClass("panel-mora-success");
    }                 

    $("#cuerpoTabla").html("<tr><td colspan='7' class='text-center'>Ingrese Usuario</td></tr>");

    moraUser = null;
    idUser = null;
    maxUser = null;
    habilitarPrestamo();
}

function limpiarMaterial(){
    $("#idMaterial").val("");
    $("#idMaterial").prop("readonly", false);
    $("#btnBuscarMaterial").prop("disabled", false);
    $("#btnVerMas").prop("disabled", true);
    $("#btnVerMas").data("documento", "");
    $("#btnPrestar").prop("disabled", true);
    
    $("#titulo").val("");
    $("#disponibilidad").val("");
    $("#totalMora").html("");

    idMaterial = null;
    dispMat = null;
    habilitarPrestamo();
}

function devolver(prestamo, ejemplar){
    if(moraUser > 0){
        mensaje("Devolución", "La mora fue cancelada");
    }

    $.ajax({
        url: "DevolverMaterial",
        type: "POST",
        data: { 
            idUs : idUser,
            idPrest : prestamo,
            idMat : ejemplar
        },
        dataType: "json",
        encode: "true"
    })
    .done(function(datos) {
        if(datos.exito == true){
            $("#btnBuscarUsuario").click();
        }
    })
    .fail(function(){
        alert("Error");
    });
}

$("#btnLimpiarUsuario").click(function (){
    limpiarUsuario();
});

$("#btnLimpiarMaterial").click(function (){
    limpiarMaterial();
});

function habilitarPrestamo(){
    if((moraUser === 0) && dispMat && (prestUser < maxUser)){
        $("#btnPrestar").prop("disabled", false);
    } else {
        $("#btnPrestar").prop("disabled", true);
    }
}
