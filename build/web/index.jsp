<%-- 
    Document   : index
    Created on : 04-24-2018, 11:31:16 AM
    Author     : José Fernando Flores Santamaría - FS150192
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="listaCategoria" class="sv.edu.udb.beans.Categoria" scope="page"/>
<% listaCategoria.getLista(); %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- <link rel="icon" href="../../favicon.ico"> -->

		<title><fmt:message key="titulo.sistema"/></title>

		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		<link href="css/dashboard.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>
        
        <jsp:include page="navBar.jsp" />
        
		<div class="container-fluid">
			<div class="row">

				<div class="col-sm-12 col-md-12 col-xs-12 main">
					<h1><fmt:message key="titulo.sistema"/></h1>
					
					<div class="divide-30"></div>

					<div class="container-fluid">
						<div class="col-md-8 col-md-offset-2 search-form-border">
							<h3><fmt:message key="busqueda.formulario"/></h3>
							<div class="divide-10"></div>
                            <form class="form-horizontal" id="busqueda" method="POST" action="resultados.jsp">
								<div class="col-md-12 form-group">
									<label class="col-md-2 col-xs-2 control-label"><fmt:message key="busqueda.tituloMat"/>: </label>
										<div class="col-md-10 col-xs-10">
											<input type="text" class="form-control" name="txtTitulo" id="txtTitulo" value=""/>
									</div>
								</div>
								<div class="col-md-12 form-group">
									<label class="col-md-2 col-xs-2 control-label"><fmt:message key="busqueda.autor"/>: </label>
										<div class="col-md-10 col-xs-10">
											<input type="text" class="form-control" name="txtAutor" id="txtAutor" value="" />
									</div>
								</div>
								<div class="col-md-12 form-group">
									<label class="col-md-2 col-xs-2 control-label"><fmt:message key="busqueda.titulo"/> </label>
										<div class="col-md-3 col-xs-10">
											<select class="form-control" id="txtTipo" name="txtTipo">
                                                <c:forEach items="${listaCategoria.lista}" var="registro">
                                                    <option value="${registro.categoria     }">${registro.categoria}</option>
                                                </c:forEach>
											</select>
									</div>
								</div>
								<div class="col-md-12 form-group">
									<div class="col-md-10 col-md-offset-2">
										<input type="submit" class="btn btn-primary pull-right" name="btnBuscar" id="btnBuscar" value="<fmt:message key="boton.buscar"/>">
									</div>              
								</div>
								<div class="divide-10"></div>
							</form>
						</div>
					</div>
                    
				</div>
			</div>
		</div>

		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/ie10-viewport-bug-workaround.js"></script>
        <script type="text/javascript">
            $(document).ready(function (){
                $("#busqueda").submit(function (event){
                    var titulo = $("#txtTitulo").val().trim();
                    var autor = $("#txtAutor").val().trim();
                    
                    if(titulo || autor ){
                       return true;
                    } else {
                        event.preventDefault();
                    }                 
                });
            });
        </script>
	</body>
</html>
    