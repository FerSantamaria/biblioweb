<%-- 
    Document   : resultados
    Created on : 02-may-2018, 16:09:31
    Author     : gabymedinacolegio
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="resultados" class="sv.edu.udb.beans.Documento" scope="page"/>
<jsp:setProperty name="resultados" property="titulo" param="txtTitulo" />
<jsp:setProperty name="resultados" property="autor" param="txtAutor" />
<jsp:setProperty name="resultados" property="tipo" param="txtTipo" />
<% resultados.getListaBusqueda(); %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- <link rel="icon" href="../../favicon.ico"> -->

		<title><fmt:message key="titulo.sistema"/></title>

		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		<link href="css/dashboard.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>

		<jsp:include page="navBar.jsp" />

        <div class="container">
            <div class="row">

                <div class="col-sm-12 col-md-12 col-xs-12">
                    <h1 class="page-header"><fmt:message key="resultado.titulo"/></h1>
                    <c:set var="i" value="0" />
                    <c:forEach items="${resultados.listaBusqueda}" var="registro">
                        <div>
                            <h4><strong>${ i = i + 1 }.</strong>
                                <a href="detalles.jsp?id=${registro.id}"> <i class="fa fa-book"></i> ${registro.titulo}</a>
                            </h4>   
                            <span>${registro.autor}</span>
                            <br>
                            <span>${registro.editorial}</span>
                        </div>
                        <div class="divide-30"></div>
                    </c:forEach>
                </div>
            </div>
		</div>

		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/ie10-viewport-bug-workaround.js"></script>
        
	</body>
</html>
