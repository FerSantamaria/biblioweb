<%-- 
    Document   : documentos
    Created on : 04-24-2018, 04:06:20 PM
    Author     : José Fernando Flores Santamaría - FS150192
--%>
<%@page session="true"%>
<%
    HttpSession sesionUsuario = request.getSession();
    
    if(sesionUsuario.getAttribute("id") == null){
        response.sendRedirect("login.jsp?error=sesion");
    } else {
        if(!sesionUsuario.getAttribute("tipo").toString().equals("1")){
            response.sendRedirect("estado.jsp");
        }
    }
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="listaDocumento" class="sv.edu.udb.beans.Documento" scope="page"/>
<% listaDocumento.getLista(); %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- <link rel="icon" href="../../favicon.ico"> -->

		<title><fmt:message key="titulo.sistema"/></title>

		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		<link href="css/dashboard.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>

        <jsp:include page="navBarAdmin.jsp" />

		<div class="container-fluid">
			<div class="row">

				<jsp:include page="sideBar.jsp" />

				<div class="col-sm-9 col-md-10 col-md-offset-2 col-sm-offset-3 col-xs-12 main">
					<h1 class="page-header"><fmt:message key="documento.titulo"/></h1>
					
					<div class="col-md-12">
						<h3><fmt:message key="documento.nuevo"/></h3>
						<a href="infoDocumento.jsp">
							<button class="btn btn-primary"><fmt:message key="documento.nuevo"/></button>
						</a>
					</div>

					<div class="divide-20"></div>

					<div class="col-md-12">
						<h3><fmt:message key="documento.registrados"/></h3>
                        <div class="table-responsive">
                            <table class="table table-striped table-responsive">
                                <tr>
                                    <th><fmt:message key="documento.id"/></th>
                                    <th><fmt:message key="documento.tituloMat"/></th>
                                    <th><fmt:message key="documento.tipo"/></th>
                                    <th><fmt:message key="documento.ubicacion"/></th>
                                    <th><fmt:message key="documento.opciones"/></th>
                                </tr>
                                <c:forEach items="${listaDocumento.lista}" var="registro">
                                    <tr>
                                        <td>${registro.id}</td>
                                        <td>${registro.titulo}</td>
                                        <td><span class="label label-success">${registro.n_categoria}</span></td>
                                        <td>${registro.ubicacion}</td>
                                        <td>
                                            <a title="<fmt:message key="ayuda.editar"/>" href="infoDocumento.jsp?action=editar&id=${registro.id}" class="btn btn-primary btn-circle"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                                            <a title="<fmt:message key="ayuda.eliminar"/>" class="btn btn-circle btn-primary" data-toggle="modal" data-target="#optionModal-${registro.contador}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                            <div class="modal fade" id="optionModal-${registro.contador}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                  <div class="modal-content">
                                                    <div class="modal-header">
                                                      <h5 class="modal-title" id="exampleModalLabel"><fmt:message key="documento.titModal"/></h5>
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                      </button>
                                                    </div>
                                                    <div class="modal-body">
                                                       <fmt:message key="documento.notif1"/>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a class="btn btn-primary" href="manto/mantoDocumento.jsp?action=eliminar&id=${registro.id}"><fmt:message key="disp.si"/></a>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><fmt:message key="disp.no"/></button>
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>
                                        </td>                                        
                                    </tr>
                                </c:forEach>
                                <!--<tr>
                                    <td>1</td>
                                    <td>Libro 1</td>
                                    <td>
                                        <span class="label label-success">Libro</span>
                                    </td>
                                    <td>100.11.1 - BCA. Central</td>
                                    <td>
                                        <a class="btn btn-default btn-circle btn-primary" title="Editar"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                                        <a class="btn btn-default btn-circle btn-primary" title="Eliminar"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                    </td>
                                </tr>-->
                            </table>
                        </div>
					</div>
				
				</div>
			</div>
		</div>

		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/ie10-viewport-bug-workaround.js"></script>
        <script src="js/util.js"></script>
	</body>
</html>
