<%-- 
    Document   : infoCategorias
    Created on : 04-24-2018, 04:01:12 PM
    Author     : Jos� Fernando Flores Santamar�a - FS150192
--%>
<%@page session="true"%>
<%
    HttpSession sesionUsuario = request.getSession();
    
    if(sesionUsuario.getAttribute("id") == null){
        response.sendRedirect("login.jsp?error=sesion");
    } else {
        if(!sesionUsuario.getAttribute("tipo").toString().equals("1")){
            response.sendRedirect("estado.jsp");
        }
    }
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="infoc" class="sv.edu.udb.beans.Categoria" scope="session"/>
<jsp:setProperty name="infoc" property="id" param="id" />
<% 
    if(request.getParameter("id") != null){
        infoc.getDatos();
    }
%>

<c:if test="${empty param}">
    <c:remove var="infoc" scope="session" />
</c:if>

<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- <link rel="icon" href="../../favicon.ico"> -->

		<title><fmt:message key="titulo.sistema"/></title>

		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		<link href="css/dashboard.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>

        <jsp:include page="navBarAdmin.jsp" />

		<div class="container-fluid">
			<div class="row">

                <jsp:include page="sideBar.jsp" />

				<div class="col-sm-9 col-md-10 col-md-offset-2 col-sm-offset-3 col-xs-12 main">
					<h1 class="page-header"><fmt:message key="categoria.titulo"/></h1>
                                        
                                        <c:if test="${not empty param.done}">
                                            <div class="alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <strong>
                                                    <c:choose>
                                                        <c:when test="${param.done == 'insertar'}" >
                                                            <fmt:message key="categoria.notif2"/>                                     
                                                        </c:when>
                                                        <c:when test="${param.done == 'editar'}" >
                                                            <fmt:message key="categoria.notif3"/>                                     
                                                        </c:when>
                                                    </c:choose>
                                                </strong>
                                            </div>
                                        </c:if>

					<!-- <div class="alert alert-danger alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<strong>�Error!</strong> Alerta de error.
					</div> -->

					<div class="col-md-12">
						<h3><fmt:message key="usuario.datos"/>: </h3>
                            <c:choose>
                                <c:when test="${empty param || param.done == 'insertar'}" >
                                    <form action="manto/mantoCategoria.jsp?action=insertar" method="POST">
                                </c:when>
                                <c:when test="${param.action == 'editar' || param.done == 'editar'}">
                                    <form action="manto/mantoCategoria.jsp?action=editar" method="POST">
                                </c:when>
                            </c:choose>                                                
							<div class="col-md-6">
								<div class="form-group">
									<label for="id"><fmt:message key="categoria.id"/>: 
										<a data-toggle="tooltip" data-placement="right" title="<fmt:message key="ayuda.id"/>">
											<i class="fa fa-question-circle"></i>
										</a>
									</label>
									<input type="text" class="form-control" id="id" name="id" required="true" readonly="true" value="<c:out value="${infoc.id}" />" />
								</div>
								<div class="form-group">
									<label for="nombre"><fmt:message key="categoria.nombre"/>: </label>
									<input type="text" class="form-control" id="categoria" name="categoria" required="true" value="<c:out value="${infoc.categoria}" />" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label><fmt:message key="categoria.campos"/>: </label>
									 <div class="checkbox">
										<label><input type="checkbox" name="isbn" ><fmt:message key="documento.isbn"/></label>
									</div>
									<div class="checkbox">
										<label><input type="checkbox" name="edicion" ><fmt:message key="documento.edicion"/></label>
									</div>
									<div class="checkbox">
										<label><input type="checkbox" name="resumen" ><fmt:message key="documento.resumen"/></label>
									</div>
									<div class="checkbox">
										<label><input type="checkbox" name="recurso" ><fmt:message key="documento.digital"/></label>
									</div>                  
								</div>
							</div>
							
							
							<div class="form-group">
								<input type="submit" class="btn btn-primary pull-right" id="btnGuardar" name="btnGuardar" value="<fmt:message key="boton.guardar"/>" />
							</div>
						</form>
					</div>
								
				</div>
			</div>
		</div>

		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/ie10-viewport-bug-workaround.js"></script>
        <script src="js/util.js"></script>
	</body>
</html>

