## BiblioWeb

___

**Librerías usadas:**

1. MySQL JDBC driver (Incluida en el proyecto).
2. Json-Simple (Incluida en el proyecto).
3. JSTL 1.2 (Incluida en el proyecto).

___

*El proyecto necesita Apache Tomcat 9 o superior, si no lo tienen está como una descarga adicional en la zona de [descargas](https://bitbucket.org/FerSantamaria/biblioweb/downloads/)*
