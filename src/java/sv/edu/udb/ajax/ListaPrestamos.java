/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.ajax;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sv.edu.udb.conexion.Conexion;

/**
 *
 * @author José Fernando Flores Santamaría - FS150192
 */
public class ListaPrestamos extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Connection con;
    ResultSet rs;
    PreparedStatement ps;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String id = request.getParameter("id");
            String tabla = request.getParameter("tabla");
            String idioma = request.getParameter("lang");
            SimpleDateFormat formatoEs = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat formatoEn = new SimpleDateFormat("MM/dd/yyyy");
            String respuesta = "";
            
            try {
                con = Conexion.getConnection();
                ps = con.prepareStatement("SELECT nueva.*, tbl_documentos.titulo, categoria.categoria FROM tbl_documentos JOIN (SELECT prestamo.id_prestamo, prestamo.id_ejemplar, tbl_ejemplares.id_documento, prestamo.fecha_inicio, prestamo.fecha_fin FROM prestamo JOIN tbl_ejemplares ON prestamo.id_ejemplar=tbl_ejemplares.id_ejemplar WHERE prestamo.id_usuario=? AND prestamo.activo = 1) AS nueva ON nueva.id_documento=tbl_documentos.id_documento JOIN categoria ON categoria.id_categoria = tbl_documentos.id_categoria");
                ps.setString(1, id);
                rs = ps.executeQuery();
                
                if( tabla != null){
                    while(rs.next()){
                        respuesta += "<tr>";
                        respuesta += "<td class='hidden-xs'>" + rs.getString("id_ejemplar") + "</td>";
                        respuesta += "<td>" + rs.getString("titulo") + "</td>";
                        respuesta += "<td><span class='label label-info'>" + rs.getString("categoria") + "</span></td>";
                        if(idioma.equals("es")){
                            respuesta += "<td>" + formatoEs.format(rs.getDate("fecha_inicio")) + "</td>";
                            respuesta += "<td>" + formatoEs.format(rs.getDate("fecha_fin")) + "</td>";
                        } else {
                            respuesta += "<td>" + formatoEn.format(rs.getDate("fecha_inicio")) + "</td>";
                            respuesta += "<td>" + formatoEn.format(rs.getDate("fecha_fin")) + "</td>";
                        }
                        respuesta += "</tr>";
                    }
                } else {
                    while(rs.next()){
                        respuesta += "<tr>";
                        respuesta += "<td class='hidden-xs'>" + rs.getString("id_prestamo") + "</td>";
                        respuesta += "<td class='hidden-xs'>" + rs.getString("id_ejemplar") + "</td>";
                        respuesta += "<td>" + rs.getString("titulo") + "</td>";
                        respuesta += "<td><span class='label label-info'>" + rs.getString("categoria") + "</span></td>";
                        if(idioma.equals("es")){
                            respuesta += "<td>" + formatoEs.format(rs.getDate("fecha_inicio")) + "</td>";
                            respuesta += "<td>" + formatoEs.format(rs.getDate("fecha_fin")) + "</td>";
                            respuesta += "<td><input type='button' class='btn btn-success btn-xs devolver' value='Devolver' data-idPrestamo='" + rs.getString("id_prestamo") + "' data-idMaterial='" + rs.getString("id_ejemplar") + "'/></td>";                        
                        } else {
                            respuesta += "<td>" + formatoEn.format(rs.getDate("fecha_inicio")) + "</td>";
                            respuesta += "<td>" + formatoEn.format(rs.getDate("fecha_fin")) + "</td>";
                            respuesta += "<td><input type='button' class='btn btn-success btn-xs devolver' value='Return' data-idPrestamo='" + rs.getString("id_prestamo") + "' data-idMaterial='" + rs.getString("id_ejemplar") + "'/></td>";  
                        }
                        respuesta += "</tr>";
                    }
                }
                
                ps.close();
                rs.close();
                con.close();

                out.print(respuesta);

            } catch (SQLException e) {
                Logger.getLogger(ListaPrestamos.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
