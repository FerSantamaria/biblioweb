/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.ajax;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;
import sv.edu.udb.beans.PasswordGenerator;
import sv.edu.udb.conexion.Conexion;

/**
 *
 * @author José Fernando Flores Santamaría - FS150192
 */
@WebServlet(name = "NuevaContra", urlPatterns = {"/NuevaContra"})
public class NuevaContra extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            PasswordGenerator pwg = new PasswordGenerator();
            Connection con = Conexion.getConnection();
            String id = request.getParameter("id");
            JSONObject jsob = new JSONObject();
            PreparedStatement ps;
            ResultSet rs;
            int cant = 0;
            String password;
            
            try {
                ps = con.prepareStatement("SELECT COUNT(*) FROM usuario WHERE id_usuario = ?");
                ps.setString(1, id);
                rs = ps.executeQuery();
                
                while(rs.next()){
                    cant = rs.getInt("COUNT(*)");                   
                }
                ps.close();
                rs.close();
                
                if( cant > 0){
                    password = PasswordGenerator.getPassword(PasswordGenerator.MINUSCULAS + PasswordGenerator.MAYUSCULAS + PasswordGenerator.ESPECIALES,10);
                    ps = con.prepareStatement("CALL update_user (?,?)");
                    ps.setString(1, id);
                    ps.setString(2, password);
                    rs = ps.executeQuery();
                    
                    jsob.put("usuario", id);
                    jsob.put("contrasena", password);
                    jsob.put("error", false);
                } else {
                    jsob.put("error", true);
                }
            } catch (SQLException e) {
                jsob.put("error", true);
            }
            
            out.print(jsob.toString());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
