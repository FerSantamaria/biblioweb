/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.ajax;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.logging.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sv.edu.udb.conexion.Conexion;

/**
 *
 * @author José Fernando Flores Santamaría - FS150192
 */
public class ListaConfiguracion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Connection con;
    ResultSet rs;
    PreparedStatement ps;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String respuesta = "";
            String idioma = request.getParameter("lang");
            
            try {
                con = Conexion.getConnection();
                ps = con.prepareStatement("SELECT * FROM tipo_usuario");
                rs = ps.executeQuery();

                while(rs.next()){
                    respuesta += "<tr>";
                    
                    if(idioma.equals("es")){
                        respuesta += "<td>" + rs.getString("tipo") + "</td>";
                    } else {
                        switch(rs.getString("tipo")){
                            case "Administrador":
                                respuesta += "<td>Librarian</td>";
                                break;
                            case "Maestro":
                                respuesta += "<td>Teacher</td>";
                                break;
                            case "Alumno":
                                respuesta += "<td>Student</td>";
                                break;
                        }
                    }
                    respuesta += "<td>" + rs.getInt("cant") + "</td>";
                    respuesta += "<td>" + rs.getInt("dias") + "</td>";
                    respuesta += "</tr>";
                }

                ps.close();
                rs.close();
                con.close();

                out.print(respuesta);

            } catch (SQLException e) {
                Logger.getLogger(ListaMora.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
