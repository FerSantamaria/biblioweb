/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.conexion;
import java.sql.*;
/**
 *
 * @author José Fernando Flores Santamaría - FS150192
 */
public class Conexion {
    private static String cadena = "jdbc:mysql://localhost:3306/biblio_cdb";    
    private static String nombreDriver = "com.mysql.jdbc.Driver";   
    private static String usuario = "root";   
    private static String contrasena = "";
    private static Connection con;
    
    public static Connection getConnection() {
        try {
            Class.forName(nombreDriver);
            try {
                con = DriverManager.getConnection(cadena, usuario, contrasena);
            } catch (SQLException ex) {
            }
        } catch (ClassNotFoundException ex) {
        }
        return con;
    }
}
