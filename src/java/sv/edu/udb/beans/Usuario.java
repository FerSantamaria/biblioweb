/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.beans;
import sv.edu.udb.conexion.Conexion;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.*;
/**
 *
 * @author José Fernando Flores Santamaría - FS150192
 */
public class Usuario {
    private String id;
    private String nombres;
    private String apellidos;
    private String contrasena;
    private int tipo;
    private double mora;
    private int prestados;
    private int maximo;
    private int dias;
    private ArrayList<Usuario> lista;
    
    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;
    
    private boolean error = false;
    private String accion;
    private String mensaje;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
    
    public int getMaximo() {
        return maximo;
    }

    public void setMaximo(int maximo) {
        this.maximo = maximo;
    }
    
    public double getMora(){
        return this.mora;
    }

    public void setMora(double mora) {
        this.mora = mora;
    }
    
    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }
    
    public String getMensaje() {
        return mensaje;
    }
    
    public int getPrestados() {
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement("SELECT COUNT(id_prestamo) AS total FROM prestamo WHERE id_usuario=? AND prestamo.activo=1");
            ps.setString(1, this.id);
            rs = ps.executeQuery();
            
            rs.next();
            this.prestados = rs.getInt("total");
            
            ps.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return this.prestados;
    }

    public void setPrestados(int prestados) {
        this.prestados = prestados;
    }
    
    public boolean getDatos(){
        boolean exito = false;
        
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement("SELECT * FROM usuario INNER JOIN tipo_usuario ON tipo_usuario.id_tipo = usuario.id_tipo WHERE id_usuario=?");
            ps.setString(1, this.id);
            rs = ps.executeQuery();
            
            while(rs.next()){
                exito = true;
                this.nombres = rs.getString("nombres");
                this.apellidos = rs.getString("apellidos");
                this.tipo = rs.getInt("id_tipo");
                this.maximo = rs.getInt("cant");
                this.dias = rs.getInt("dias");
            }
            
            ps.close();
            rs.close();
            con.close();
            
            getValorMora();
            
        } catch (SQLException e) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return exito;
    }
    
    public void getValorMora(){
        try {
            ArrayList<String> prestamo = new ArrayList<>();
            ArrayList<Long> diasPaga = new ArrayList<>();
            int anio = Calendar.getInstance().get(Calendar.YEAR);
            Date hoy = new Date();
            
            con = Conexion.getConnection();
            ps = con.prepareStatement("SELECT costo FROM costo_mora WHERE anio=?");
            ps.setInt(1, anio);
            rs = ps.executeQuery();
            
            rs.next();
            double costo = rs.getDouble("costo");
            
            ps.close();
            rs.close();
            
            ps = con.prepareStatement("SELECT * FROM prestamo WHERE activo=1 AND fecha_fin < CURDATE() AND id_usuario=?");
            ps.setString(1, this.id);
            
            rs = ps.executeQuery();
            rs.beforeFirst();
            while (rs.next()) {
                long milis = hoy.getTime() - rs.getDate("ultimo_calculo").getTime();
                long dias = milis / (24 * 60 * 60 * 1000);
                diasPaga.add(dias);
                prestamo.add(rs.getString("id_prestamo"));
            }
            
            ps.close();
            rs.close();
            
            for (int i=0; i < prestamo.size(); i++) {
                ps = con.prepareStatement("UPDATE mora SET mora.mora = mora.mora + ? WHERE id_prestamo=?");
                ps.setDouble(1, diasPaga.get(i) * costo);
                ps.setString(2, prestamo.get(i));
                ps.executeUpdate();
            }
            
            ps.close();
            
            ps = con.prepareStatement("SELECT SUM(mora.mora) AS total FROM mora WHERE mora.id_prestamo IN (SELECT prestamo.id_prestamo FROM prestamo WHERE prestamo.id_usuario = ?)");
            ps.setString(1, this.id);
            rs = ps.executeQuery();
            
            rs.next();
            
            this.mora = rs.getDouble("total");
        } catch (SQLException e) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public ArrayList<Usuario> getLista(){
        lista = new ArrayList<>();
        
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement("SELECT * FROM usuario");
            rs = ps.executeQuery();
            
            while(rs.next()){
                Usuario temp = new Usuario();
                temp.setId(rs.getString("id_usuario"));
                temp.setNombres(rs.getString("nombres"));
                temp.setApellidos(rs.getString("apellidos"));
                temp.setTipo(rs.getInt("id_tipo"));
                lista.add(temp);
            }
            
            ps.close();
            rs.close();
            con.close();
            
            //getValorMora();
            
        } catch (SQLException e) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, e);
        }
        return lista;
    }
    
    public boolean getInsertar(){
        boolean exito = false;
        
        if (this.nombres != null && this.apellidos != null) {
            try 
            {
                String strQuery;
                String password = PasswordGenerator.getPassword(PasswordGenerator.MINUSCULAS + PasswordGenerator.MAYUSCULAS + PasswordGenerator.ESPECIALES,10);

                con = Conexion.getConnection();            
                ps = con.prepareStatement("CALL create_user(?, ?, ?, ?)");
                ps.setString(1, this.nombres);
                ps.setString(2, this.apellidos);
                ps.setString(3, password);
                ps.setInt(4, this.tipo);

                rs = ps.executeQuery();
                while(rs.next()){
                    exito = true;
                    this.id = rs.getString(1);
                    this.contrasena = password;
                }

                con.close();
            } catch (SQLException ex) {
                error = true;
                mensaje = "Ocurrió un error al ingresar el nuevo usuario :(";
                Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return exito;
    }
    
    public boolean getEdicion(){
        boolean exito = false;
        
        if (this.nombres != null && this.apellidos != null) {
            try 
            {
                con = Conexion.getConnection();            
                ps = con.prepareStatement("UPDATE usuario set nombres=?, apellidos=?, id_tipo=? WHERE id_usuario=?");
                ps.setString(1, this.nombres);
                ps.setString(2, this.apellidos);
                ps.setInt(3, this.tipo);
                ps.setString(4, this.id);
                
                int cant = ps.executeUpdate();
                if (cant > 0) {
                    exito = true;
                }

                con.close();
            } catch (SQLException ex) {
                error = true;
                mensaje = "Ocurrió un error al ingresar el nuevo usuario :(";
                Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return exito;
    }
    
    public boolean getEliminacion(){
        boolean exito = false;
        
        if (this.id != null) {
            try 
            {
                con = Conexion.getConnection();            
                ps = con.prepareStatement("DELETE FROM usuario WHERE id_usuario=? LIMIT 1");
                ps.setString(1, this.id);
                
                int cant = ps.executeUpdate();
                if (cant > 0) {
                    exito = true;
                }

                con.close();
            } catch (SQLException ex) {
                error = true;
                mensaje = "Ocurrió un error al eliminar al usuario :(";
                Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return exito;
    }
    
    public boolean getSesion(){
        boolean exito = true;
        
        if (this.id != null && this.contrasena != null) {
            try 
            {
                con = Conexion.getConnection();            
                ps = con.prepareStatement("SELECT * FROM usuario WHERE id_usuario=? AND contrasena=SHA2(?,256)");
                ps.setString(1, this.id);
                ps.setString(2, this.contrasena);
                
                rs = ps.executeQuery();
                while (rs.next()) {
                    exito = true;
                }
                
                rs.close();
                ps.close();
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return exito;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public boolean isError() {
        return error;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }

}
