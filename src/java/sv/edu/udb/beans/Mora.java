/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.beans;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.*;
import sv.edu.udb.conexion.Conexion;
/**
 *
 * @author José Fernando Flores Santamaría - FS150192
 */
public class Mora {
    private String id;
    private int anio;
    private double costo;
    private ArrayList<Mora> lista;
    
    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }
    
    public ArrayList<Mora> getLista(){
        lista = new ArrayList<>();
        
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement("SELECT * FROM usuario");
            rs = ps.executeQuery();
            
            while(rs.next()){
                Mora temp = new Mora();
                temp.setId(rs.getString("id_mora"));
                temp.setAnio(rs.getInt("anio"));
                temp.setCosto(rs.getDouble("costo"));
                lista.add(temp);
            }
            
            ps.close();
            rs.close();
            con.close();
            
        } catch (SQLException e) {
            Logger.getLogger(Mora.class.getName()).log(Level.SEVERE, null, e);
        }
        return lista;
    }
    
    public boolean getEdicion(){
        boolean exito = false, existe=false;
        anio = Calendar.getInstance().get(Calendar.YEAR);
        
        try 
        {
            if(costo > 0){
                con = Conexion.getConnection();            
                ps = con.prepareStatement("SELECT * FROM costo_mora WHERE anio = ?");
                ps.setInt(1, this.anio);

                rs = ps.executeQuery();
                if (rs.isBeforeFirst()) {
                    existe = true;
                }
                rs.close();
                ps.close();
                con.close();

                if(!existe){
                    try 
                    {
                        con = Conexion.getConnection();            
                        ps = con.prepareStatement("INSERT INTO costo_mora (anio, costo) VALUES (?, ?)");
                        ps.setInt(1, this.anio);
                        ps.setDouble(2, this.costo);

                        int cant = ps.executeUpdate();
                        if (cant > 0) {
                            exito = true;
                        }

                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(Mora.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Mora.class.getName()).log(Level.SEVERE, null, ex);
        }

        return exito;
    }
}
