/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.beans;

import java.sql.*;
import java.util.logging.*;
import sv.edu.udb.conexion.Conexion;

/**
 *
 * @author José Fernando Flores Santamaría - FS150192
 */
public class Configuracion {
    private String id;
    private int cant;
    private int dias;
    
    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCant() {
        return cant;
    }

    public void setCant(int cant) {
        this.cant = cant;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }
    
    public boolean getEdicion(){
        boolean exito = false;
        
        if (this.id != null && this.dias > 0 && this.cant > 0) {
            try 
            {
                con = Conexion.getConnection();            
                ps = con.prepareStatement("UPDATE tipo_usuario SET cant = ?, dias = ? WHERE id_tipo=?");
                ps.setInt(1, this.cant);
                ps.setInt(2, this.dias);
                ps.setString(3, this.id);
                
                int cant = ps.executeUpdate();
                if (cant > 0) {
                    exito = true;
                }

                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(Configuracion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return exito;
    }
}
