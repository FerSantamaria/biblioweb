/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.beans;
import java.sql.*;
import java.util.logging.*;
import sv.edu.udb.conexion.Conexion;
/**
 *
 * @author José Fernando Flores Santamaría - FS150192
 */
public class Reportes {
    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;
    private String masPrestados = "";

    public String getMasPrestados() {
        try {
            int num = 0;
            con = Conexion.getConnection();
            ps = con.prepareStatement("SELECT tbl_documentos.titulo, prestados.veces FROM tbl_documentos JOIN (SELECT tbl_ejemplares.id_documento , count(*) AS veces FROM prestamo JOIN tbl_ejemplares ON prestamo.id_ejemplar=tbl_ejemplares.id_ejemplar GROUP BY tbl_ejemplares.id_documento HAVING count(*) > 0) AS prestados ON tbl_documentos.id_documento = prestados.id_documento ORDER By prestados.veces DESC");
            rs = ps.executeQuery();
            
            while(rs.next()){
                masPrestados += "<tr>";
                masPrestados += "<td>" + ++num + "</td>";
                masPrestados += "<td>" + rs.getString("titulo") + "</td>";
                masPrestados += "<td>" + rs.getString("veces") + "</td>";
                masPrestados += "</tr>";
            }
            
            ps.close();
            rs.close();
            con.close();
            
        } catch (SQLException e) {
            Logger.getLogger(Mora.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return masPrestados;
    }

        
}
