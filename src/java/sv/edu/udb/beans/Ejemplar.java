/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.beans;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.*;
import sv.edu.udb.conexion.Conexion;

/**
 *
 * @author José Fernando Flores Santamaría - FS150192
 */
public class Ejemplar {
    private String id;
    private String idDocumento;
    private boolean disponibilidad;
    private String notas;
    
    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;
    
    private ArrayList<Ejemplar> lista;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(String idDocumento) {
        this.idDocumento = idDocumento;
    }

    public boolean getDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(boolean disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }
    
    public void getDatos(){
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement("SELECT * FROM tbl_ejemplares WHERE id_ejemplar=?");
            ps.setString(1, this.id);
            rs = ps.executeQuery();
            
            while(rs.next()){
                this.idDocumento = rs.getString("id_documento");
                this.disponibilidad = rs.getBoolean("disponibilidad");
                this.notas = rs.getString("notas");
            }
            
            ps.close();
            rs.close();
            con.close();
            
        } catch (SQLException e) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public ArrayList<Ejemplar> getLista(){
        lista = new ArrayList<>();
        
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement("SELECT * FROM tbl_ejemplares WHERE id_documento=?");
            ps.setString(1, this.idDocumento);
            rs = ps.executeQuery();
            
            while(rs.next()){
                Ejemplar temp = new Ejemplar();
                temp.setId(rs.getString("id_ejemplar"));
                temp.setDisponibilidad(rs.getBoolean("disponibilidad"));
                lista.add(temp);
            }
            
            ps.close();
            rs.close();
            con.close();
            
        } catch (SQLException e) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return lista;
    }
}
