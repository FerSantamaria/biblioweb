/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.beans;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.*;
import sv.edu.udb.conexion.Conexion;
/**
 *
 * @author José Fernando Flores Santamaría - FS150192
 */
public class Prestamo {
    private String id;
    private String idEjemplar;
    private String idUsuario;
    private Date fechaInicio;
    private Date fechaFin;
    private boolean activo;
    private Date ultimoCalculo;
    private int dias;
    
    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;
    
    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat formato2 = new SimpleDateFormat("yyyy-MM-dd");
    Date hoy = new Date();
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdEjemplar() {
        return idEjemplar;
    }

    public void setIdEjemplar(String idEjemplar) {
        this.idEjemplar = idEjemplar;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Date getUltimoCalculo() {
        return ultimoCalculo;
    }

    public void setUltimoCalculo(Date ultimoCalculo) {
        this.ultimoCalculo = ultimoCalculo;
    }
    
    public void getDatos(){
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement("SELECT * FROM prestamo WHERE id_prestamo=?");
            ps.setString(1, this.id);
            rs = ps.executeQuery();
            
            while(rs.next()){
                this.idEjemplar = rs.getString("id_ejemplar");
                this.idUsuario = rs.getString("id_usuario");
                this.fechaInicio = rs.getDate("fecha_inicio");
                this.fechaFin = rs.getDate("fecha_fin");
                this.activo = rs.getBoolean("activo");
                this.ultimoCalculo = rs.getDate("ultimo_calculo");
            }
            
            ps.close();
            rs.close();
            con.close();
            
        } catch (SQLException e) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public boolean getDevolucion(){
        boolean exito = false;
        ArrayList<Integer> prestamos = new  ArrayList<>();
        
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement("SELECT prestamo.id_prestamo FROM prestamo WHERE prestamo.id_usuario = ? AND prestamo.activo = 1");
            ps.setString(1, this.idUsuario);
            rs = ps.executeQuery();
            
            while (rs.next()) {  
                prestamos.add(rs.getInt("id_prestamo"));
            }
            
            ps.close();
            rs.close();
            
            for (int i = 0; i < prestamos.size(); i++) {
                ps = con.prepareStatement("UPDATE mora SET mora.mora=0 WHERE id_prestamo=?");
                ps.setInt(1, prestamos.get(i));
                ps.executeUpdate();
            }
            
            ps = con.prepareStatement("UPDATE prestamo SET prestamo.activo=0 WHERE id_prestamo=?");
            ps.setString(1, this.id);
            ps.executeUpdate();
            
            ps = con.prepareStatement("UPDATE tbl_ejemplares SET tbl_ejemplares.disponibilidad=1 WHERE id_ejemplar=?");
            ps.setString(1, this.idEjemplar);
            ps.executeUpdate();
            
            exito = true;
            ps.close();
            rs.close();
            con.close();
            
        } catch (SQLException e) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return exito;
    }
    
    public boolean getPrestar(){
        boolean exito = false;
        
        try {
            con = Conexion.getConnection();
            String fi = formato2.format(hoy);
            String ff = fechaEntrega(this.dias);
            ps = con.prepareStatement("INSERT INTO prestamo (id_ejemplar, id_usuario, fecha_inicio, fecha_fin, activo, ultimo_calculo) VALUES (?,?,?,?,1,?)");
            ps.setString(1, this.idEjemplar);
            ps.setString(2, this.idUsuario);
            ps.setString(3, fi);
            ps.setString(4, ff);
            ps.setString(5, ff);
            
            int cant = ps.executeUpdate();
            
            if(cant > 0){
                exito = true;
            }
            
            ps.close();
            con.close();
            
        } catch (SQLException e) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return exito;
    }
    
    private String fechaEntrega(int maximo){
        String fecha = "";
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // Now use today date.
        c.add(Calendar.DATE, maximo); // Adding 5 days
        fecha = formato2.format(c.getTime());
        return fecha;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }
}
