/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import sv.edu.udb.conexion.Conexion;

/**
 *
 * @author malfa
 */
public class Categoria {

    /**
     * @return the error
     */
    public boolean isError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(boolean error) {
        this.error = error;
    }

    /**
     * @return the accion
     */
    public String getAccion() {
        return accion;
    }

    /**
     * @param accion the accion to set
     */
    public void setAccion(String accion) {
        this.accion = accion;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;
    
    private String id;
    private String categoria;
    private boolean isbn;
    private boolean edicion;
    private boolean resumen;
    private boolean recurso;
    private ArrayList lista;
    
    private boolean error = false;
    private String accion;
    private String mensaje;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the categoria
     */
    public String getCategoria() {
        return categoria;
    }

    /**
     * @param categoria the categoria to set
     */
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    /**
     * @return the isbn
     */
    public boolean isIsbn() {
        return isbn;
    }

    /**
     * @param isbn the isbn to set
     */
    public void setIsbn(boolean isbn) {
        this.isbn = isbn;
    }

    /**
     * @return the edicion
     */
    public boolean isEdicion() {
        return edicion;
    }

    /**
     * @param edicion the edicion to set
     */
    public void setEdicion(boolean edicion) {
        this.edicion = edicion;
    }

    /**
     * @return the resumen
     */
    public boolean isResumen() {
        return resumen;
    }

    /**
     * @param resumen the resumen to set
     */
    public void setResumen(boolean resumen) {
        this.resumen = resumen;
    }

    /**
     * @return the recurso
     */
    public boolean isRecurso() {
        return recurso;
    }

    /**
     * @param recurso the recurso to set
     */
    public void setRecurso(boolean recurso) {
        this.recurso = recurso;
    }
    
    public boolean getInsertar(){
        boolean exito = false;
        
        if (this.categoria != null) {
            try 
            {

                con = Conexion.getConnection();            
                ps = con.prepareStatement("INSERT INTO `biblio_cdb`.`categoria` (`categoria`, `isbn`, `edicion`, `resumen`, `recurso_digital`) VALUES (?, ?, ?, ?, ?);");
                ps.setString(1, this.categoria);
                ps.setBoolean(2, this.isbn);
                ps.setBoolean(3, this.edicion);
                ps.setBoolean(4, this.resumen);
                ps.setBoolean(5, this.recurso);
                ps.executeUpdate();

                con.close();
            } catch (SQLException ex) {
                setError(true);
                setMensaje("Ocurrió un error al ingresar una nueva categoría :(");
                Logger.getLogger(Categoria.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return exito;
    }
    
    public boolean getDatos(){
        boolean exito = false;
        
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement("SELECT * FROM categoria WHERE id_categoria=?");
            ps.setString(1, this.id);
            rs = ps.executeQuery();
            
            while(rs.next()){
                exito = true;
                this.id = rs.getString("id_categoria");
                this.categoria = rs.getString("categoria");
                this.isbn = rs.getBoolean("isbn");
                this.edicion = rs.getBoolean("edicion");
                this.resumen = rs.getBoolean("resumen");
                this.recurso = rs.getBoolean("recurso_digital");
            }
            
            ps.close();
            rs.close();
            con.close();
            
            
        } catch (SQLException e) {
            Logger.getLogger(Categoria.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return exito;
    }
    
    public void getEliminar(){
        boolean exito = false;
        
        if (this.id != null) {
            try 
            {
                con = Conexion.getConnection();            
                ps = con.prepareStatement("delete from categoria where id_categoria = ?");
                ps.setString(1, this.id);
                
                int cant = ps.executeUpdate();
                if (cant > 0) {
                    exito = true;
                }

                con.close();
            } catch (SQLException ex) {
                setError(true);
                setMensaje("Ocurrió un error al eliminar la categoría :(");
                Logger.getLogger(Categoria.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public boolean getEditar(){
        boolean exito = false;
        
        if (this.categoria != null) {
            try 
            {
                con = Conexion.getConnection();            
                ps = con.prepareStatement("UPDATE `categoria` SET `categoria` = ?, `isbn` = ?, `edicion` = ?, `resumen` = ?, `recurso_digital` = ? WHERE `id_categoria` = ?");
                ps.setString(1, this.categoria);
                ps.setBoolean(2, this.isbn);
                ps.setBoolean(3, this.edicion);
                ps.setBoolean(4, this.resumen);
                ps.setBoolean(5, this.recurso);
                ps.setString(6, this.id);
                
                int cant = ps.executeUpdate();
                if (cant > 0) {
                    exito = true;
                }

                con.close();
            } catch (SQLException ex) {
                error = true;
                mensaje = "Ocurrió un error al ingresar el nuevo usuario :(";
                Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return exito;
    }
    
    public ArrayList<Categoria> getLista(){
        lista = new ArrayList<>();
        
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement("SELECT * FROM categoria");
            rs = ps.executeQuery();
            
            while(rs.next()){
                Categoria temp = new Categoria();
                temp.setId(rs.getString("id_categoria"));
                temp.setCategoria(rs.getString("categoria"));
                lista.add(temp);
            }
            
            ps.close();
            rs.close();
            con.close();
            
            //getValorMora();
            
        } catch (SQLException e) {
            Logger.getLogger(Categoria.class.getName()).log(Level.SEVERE, null, e);
        }
        return lista;
    }
    
}
