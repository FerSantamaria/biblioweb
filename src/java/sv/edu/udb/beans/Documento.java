/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.beans;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.*;
import sv.edu.udb.conexion.Conexion;

/**
 *
 * @author José Fernando Flores Santamaría - FS150192
 */
public class Documento {    
        
    private String id;
    private int categoria;
    private String n_categoria;
    private String titulo;
    private String edicion;
    private String editorial;
    private String descripcion;
    private String resumen;
    private String recurso;
    private String contenido;
    private String temas;
    private String isbn;
    private String ubicacion;
    private Date fecha;
    private String notas;
    private int cantidad;
    private String autor;
    private String tipo;
    
    private int contador;
    
    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;
    
    private ArrayList lista;
    private ArrayList listaBusqueda;
    
    private boolean error = false;
    private String accion;
    private String mensaje;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCategoria() {
        return categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getEdicion() {
        return edicion;
    }

    public void setEdicion(String edicion) {
        this.edicion = edicion;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getResumen() {
        return resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    public String getRecurso() {
        return recurso;
    }

    public void setRecurso(String recurso) {
        this.recurso = recurso;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getTemas() {
        return temas;
    }

    public void setTemas(String temas) {
        this.temas = temas;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    /**
     * @return the notas
     */
    public String getNotas() {
        return notas;
    }

    /**
     * @param notas the notas to set
     */
    public void setNotas(String notas) {
        this.notas = notas;
    }

    /**
     * @return the cantidad
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
    /**
     * @return the n_categoria
     */
    public String getN_categoria() {
        return n_categoria;
    }

    /**
     * @param n_categoria the n_categoria to set
     */
    public void setN_categoria(String n_categoria) {
        this.n_categoria = n_categoria;
    }
    
    /**
     * @return the contador
     */
    public int getContador() {
        return contador;
    }

    /**
     * @param contador the contador to set
     */
    public void setContador(int contador) {
        this.contador = contador;
    }    
    
    /**
     * @return the error
     */
    public boolean isError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(boolean error) {
        this.error = error;
    }

    /**
     * @return the accion
     */
    public String getAccion() {
        return accion;
    }

    /**
     * @param accion the accion to set
     */
    public void setAccion(String accion) {
        this.accion = accion;
    }
    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
        public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    public void getDatos(){
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement("SELECT * FROM tbl_documentos WHERE id_documento=?");
            ps.setString(1, this.id);
            rs = ps.executeQuery();
            
            Connection con1 = Conexion.getConnection();
            PreparedStatement ps1;
            ResultSet rs1;
            
            while(rs.next()){
                this.id = rs.getString("id_documento");
                this.categoria = rs.getInt("id_categoria");
                this.titulo = rs.getString("titulo");
                this.edicion = rs.getString("edicion");
                this.editorial = rs.getString("imp");
                this.descripcion = rs.getString("descripcion");
                this.resumen = rs.getString("resumen");
                this.recurso = rs.getString("recurso_digital");
                this.contenido = rs.getString("contenido");
                this.temas = rs.getString("temas");
                this.isbn = rs.getString("isbn");
                this.ubicacion = rs.getString("ubicacion");
                this.fecha = rs.getDate("fecha_agregado");
                ps1 = con.prepareStatement("SELECT * FROM autor WHERE id_documento = ?");
                ps1.setString(1, this.id);
                rs1 = ps1.executeQuery();
                this.autor = "";
                while(rs1.next()){
                    this.autor = this.autor + rs1.getString("autor") + "\n";
                }
                ps1.close();
                con1.close();
                rs1.close();
            }
            
            ps.close();
            rs.close();
            con.close();
            
        } catch (SQLException e) {
            Logger.getLogger(Documento.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void getInsertar(){
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement("call newDocument (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            ps.setInt(1, this.categoria);
            ps.setString(2, this.titulo);
            ps.setString(3, this.edicion);
            ps.setString(4, this.editorial);
            ps.setString(5, this.descripcion);
            ps.setString(6, this.resumen);
            ps.setString(7, this.recurso);
            ps.setString(8, this.contenido);
            ps.setString(9, this.temas);
            ps.setString(10, this.isbn);
            ps.setString(11, this.ubicacion);
            ps.setInt(12, this.cantidad);
            ps.setString(13, this.notas);
            ps.executeUpdate();              
            
            ps.close();
            con.close();            
            
            String[] autores = getAutor().split("\\n");
           PreparedStatement[] ps1 = new PreparedStatement[autores.length];
            Connection[] con1 = new Connection[autores.length];
            int counter = 0;
            for(String autor : autores)
            {
                con1[counter] = Conexion.getConnection();
                ps1[counter] = con1[counter].prepareStatement("insert into autor (id_documento, autor) values ((SELECT id_documento FROM tbl_documentos where fecha_agregado = (SELECT max(fecha_agregado) from tbl_documentos)), ?)");
                ps1[counter].setString(1, autor);
                ps1[counter].executeUpdate();
                ps1[counter].close();
                con1[counter].close();
                counter++;
            }
            
        } catch (SQLException e) {
            Logger.getLogger(Documento.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public boolean getEditar(){
        boolean exito = false;
        
        if (this.titulo != null) {
            try 
            {
                con = Conexion.getConnection();            
                ps = con.prepareStatement("UPDATE `tbl_documentos` SET `id_categoria` = ?, `titulo` = ?, `edicion` = ?, `imp` = ?,  `descripcion` = ?, `resumen` = ?, `recurso_digital` = ?, `contenido` = ?, `temas` = ?, `isbn` = ?, `ubicacion` = ? WHERE `id_documento` = ?;");
                ps.setInt(1, this.categoria);
                ps.setString(2, this.titulo);
                ps.setString(3, this.edicion);
                ps.setString(4, this.editorial);
                ps.setString(5, this.descripcion);
                ps.setString(6, this.resumen);
                ps.setString(7, this.recurso);
                ps.setString(8, this.contenido);
                ps.setString(9, this.temas);
                ps.setString(10, this.isbn);
                ps.setString(11, this.ubicacion);
                
                int cant = ps.executeUpdate();
                if (cant > 0) {
                    exito = true;
                }

                con.close();
            } catch (SQLException ex) {
                error = true;
                mensaje = "Ocurrió un error al ingresar el nuevo usuario :(";
                Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return exito;
    }
    
    /*public void getEditar(){
        try {
            con = Conexion.getConnection();
                ps = con.prepareStatement("UPDATE `tbl_documentos` SET `id_categoria` = ?, `titulo` = ?, `edicion` = ?, `imp` = ?,  `descripcion` = ?, `resumen` = ?, "
                    + "`recurso_digital` = ?, `contenido` = ?, `temas` = ?, `isbn` = ?, `ubicacion` = ? WHERE `id_documento` = ?;");                
                ps.setInt(1, this.categoria);
                ps.setString(2, this.titulo);
                ps.setString(3, this.edicion);
                ps.setString(4, this.editorial);
                ps.setString(5, this.descripcion);
                ps.setString(6, this.resumen);
                ps.setString(7, this.recurso);
                ps.setString(8, this.contenido);
                ps.setString(9, this.temas);
                ps.setString(10, this.isbn);
                ps.setString(11, this.ubicacion);
                ps.setString(14, this.id);
                ps.executeUpdate();
            
            ps.close();
            con.close();
            
        } catch (SQLException e) {
            Logger.getLogger(Documento.class.getName()).log(Level.SEVERE, null, e);
        }
    }*/
    
    public void getEliminar(){
        boolean exito = false;
        
        if (this.id != null) {
            try 
            {
                con = Conexion.getConnection();            
                    ps = con.prepareStatement("delete from tbl_documentos where id_documento = ?");
                ps.setString(1, this.id);
                
                int cant = ps.executeUpdate();
                if (cant > 0) {
                    exito = true;
                }

                con.close();
            } catch (SQLException ex) {
                setError(true);
                setMensaje("Ocurrió un error al eliminar al documento :(");
                Logger.getLogger(Documento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public ArrayList<Documento> getLista(){
        lista = new ArrayList<>();
        
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement("SELECT doc.id_documento, doc.titulo, cat.categoria, doc.ubicacion FROM tbl_documentos doc left join categoria cat on cat.id_categoria = doc.id_categoria");
            rs = ps.executeQuery();
            this.contador = 0;
            
            while(rs.next()){
                Documento temp = new Documento();
                temp.setId(rs.getString("id_documento"));
                temp.setTitulo(rs.getString("titulo"));
                temp.setN_categoria(rs.getString("categoria"));
                temp.setUbicacion(rs.getString("ubicacion"));
                temp.setContador(this.contador);
                this.contador = this.contador + 1;
                lista.add(temp);
            }
            
            ps.close();
            rs.close();
            con.close();
            
            //getValorMora();
            
        } catch (SQLException e) {
            Logger.getLogger(Documento.class.getName()).log(Level.SEVERE, null, e);
        }
        return lista;
    }
    
    public ArrayList<Documento> getListaBusqueda(){
        listaBusqueda = new ArrayList<>();
        String titu = (this.titulo == null) ? "" : this.titulo;
        String aut = (this.autor == null) ? "" : this.autor;
        
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement("SELECT tbl_documentos.id_documento,tbl_documentos.titulo, autores, tbl_documentos.imp FROM tbl_documentos JOIN (SELECT GROUP_CONCAT(autor.autor) AS autores, id_documento FROM autor GROUP BY autor.id_documento) AS tbl_autores ON tbl_documentos.id_documento=tbl_autores.id_documento JOIN categoria ON tbl_documentos.id_categoria= categoria.id_categoria WHERE tbl_documentos.titulo LIKE ? AND autores LIKE ? AND categoria=?");
            ps.setString(1, "%"+titu+"%");
            ps.setString(2, "%"+aut+"%");
            ps.setString(3, this.tipo);
            rs = ps.executeQuery();
            this.contador = 0;
            
            while(rs.next()){
                Documento temp = new Documento();
                temp.setId(rs.getString("id_documento"));
                temp.setTitulo(rs.getString("titulo"));
                temp.setAutor(rs.getString("autores"));
                temp.setEditorial(rs.getString("imp"));
                listaBusqueda.add(temp);
            }
            
            ps.close();
            rs.close();
            con.close();
            
        } catch (SQLException e) {
            Logger.getLogger(Documento.class.getName()).log(Level.SEVERE, null, e);
        }
        return listaBusqueda;
    }
}
