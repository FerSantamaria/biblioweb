<%-- 
    Document   : navBar
    Created on : 04-24-2018, 11:53:14 AM
    Author     : José Fernando Flores Santamaría - FS150192
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.jsp">
                <img src="imagenes/logo.png" height="37px" width="105px" class="img-responsive logo" alt="<fmt:message key="titulo.sistema"/>">
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <c:choose>
                    <c:when test="${not empty id}">
                        <c:choose>
                            <c:when test="${tipo == 1}">
                                <li><a href="menu.jsp"><fmt:message key="titulo.admin"/></a></li>
                            </c:when>
                            <c:when test="${tipo == 2 || tipo == 3}">
                                <li><a href="estado.jsp"><fmt:message key="titulo.estado"/></a></li>
                            </c:when>
                        </c:choose>
                        <li><a href="cerrar.jsp"><fmt:message key="navbar.salir"/></a></li>
                    </c:when>
                    <c:when test="${empty id}">
                        <li><a href="login.jsp"><fmt:message key="navbar.iniciar"/></a></li>
                    </c:when>
                </c:choose>
            </ul>
        </div>
    </div>
</nav>
