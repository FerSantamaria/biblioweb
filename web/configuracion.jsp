<%-- 
    Document   : configuracion
    Created on : 05-04-2018, 08:48:58 AM
    Author     : José Fernando Flores Santamaría - FS150192
--%>
<%@page session="true"%>
<%
    HttpSession sesionUsuario = request.getSession();
    
    if(sesionUsuario.getAttribute("id") == null){
        response.sendRedirect("login.jsp?error=sesion");
    } else {
        if(!sesionUsuario.getAttribute("tipo").toString().equals("1")){
            response.sendRedirect("estado.jsp");
        }
    }
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- <link rel="icon" href="../../favicon.ico"> -->

		<title><fmt:message key="titulo.sistema"/></title>
        
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		<link href="css/dashboard.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>

		<jsp:include page="navBarAdmin.jsp" />

		<div class="container-fluid">
			<div class="row">

                <jsp:include page="sideBar.jsp" />

				<div class="col-sm-9 col-md-10 col-md-offset-2 col-sm-offset-3 col-xs-12 main">
					<h1 class="page-header"><fmt:message key="config.titulo"/></h1>
                    
                    <c:if test="${not empty param.done}">
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong><fmt:message key="config.notif1"/></strong>
                        </div>
                    </c:if>
                
                    <h3><fmt:message key="usuario.titulo"/></h3>
                    <form action="manto/mantoConfig.jsp?action=editar" method="POST">
                        <div class="form-group col-md-3">
                            <label for="rol"><fmt:message key="usuario.tipo"/>: </label>
                            <select class="form-control" id="rol" name="rol" readonly="true">
                                <option value="1"><fmt:message key="tipo.admin"/></option>
                                <option value="2"><fmt:message key="tipo.maestro"/></option>
                                <option value="3"><fmt:message key="tipo.alumno"/></option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nombre"><fmt:message key="config.ejemplares"/>: </label>
                            <input type="number" class="form-control" id="ejemplares" name="ejemplares" min="1" value="1" required="true"/>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nombre"><fmt:message key="config.dias"/>: </label>
                            <input type="number" class="form-control" id="dias" name="dias" min="1" value="1" required="true"/>
                        </div>
                        <div class="form-group col-md-3">
                            <div class="divide-25"></div>
                            <input type="submit" class="btn btn-primary pull-right" id="btnGuardarDatos" name="btnGuardarDatos" value="<fmt:message key="boton.guardar"/>" />
                        </div>
                    </form>
                    
                    <div class="clearfix divide-20"></div>
                        
                    <div class="table-responsive col-md-6">
                        <table id="tablaConf" class="table table-striped">
                            <thead>
                                <tr>
                                    <th><fmt:message key="usuario.tipo"/></th>
                                    <th><fmt:message key="config.ejemplares"/></th>
                                    <th><fmt:message key="config.dias"/></th>
                                </tr>
                            </thead>
                            <tbody id="cuerpoTablaDatos" name="cuerpoTablaDatos">
                                <tr><td colspan='2' class='text-center'><fmt:message key="config.notif2"/></td></tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="divide-20"></div>
                    
                    <h3><fmt:message key="config.mora"/></h3>
                    <form action="manto/mantoMora.jsp?action=insertar" method="POST">
                        <div class="form-group col-md-3 col-xs-8 col-sm-8">
                            <label for="nombre"><fmt:message key="config.mora"/> ($): </label>
                            <input type="number" class="form-control" id="costo" name="costo" min="0.01" step="0.01" required="true"/>
                        </div>
                        <div class="form-group col-md-3 col-xs-4 col-sm-4">
                            <div class="divide-25"></div>
                            <input type="submit" class="btn btn-primary pull-right" id="btnGuardarMora" name="btnGuardarMora" value="<fmt:message key="boton.guardar"/>" />
                        </div>
                        <div class="clearfix divide-20"></div>
                        
                        <div class="table-responsive col-md-6">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th><fmt:message key="config.anio"/></th>
                                        <th><fmt:message key="config.valor"/> ($)</th>
                                    </tr>
                                </thead>
                                <tbody id="cuerpoTabla" name="cuerpoTabla">
                                    <tr><td colspan='2' class='text-center'><fmt:message key="config.notif2"/></td></tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
				</div>
			</div>
		</div>

		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/ie10-viewport-bug-workaround.js"></script>
        <script src="js/util.js"></script>
        <script type="text/javascript">
            var cant = [];
            var dias = [];
            var idioma = "<fmt:message key="util.idioma"/>";
            
            $(document).ready(function(){
                
                $.ajax({
                    url: "ListaMora",
                    type: "POST",
                    dataType: "html",
                    encode: "true"
                })
                .done(function(datos) {
                    $("#cuerpoTabla").html(datos);
                    var fecha = new Date();
                    var anio = fecha.getFullYear();
                    $("table tr").each(function(index){
                        if(index != 0){
                            $row = $(this);
                            var reg = $row.find("td:first").text();
                            if(reg == anio){
                                $("#costo").prop("readonly", true);
                                $("#btnGuardarMora").prop("disabled", true);
                            }
                        }
                    });                   
                })
                .fail(function(){
                    $("#cuerpoTabla").html("<tr><td colspan='2' class='text-center'><fmt:message key="config.notif2"/></td></tr>");
                    alert("Error");
                });
                
                $.ajax({
                    url: "ListaConfiguracion",
                    type: "POST",
                    data: { 
                        lang : idioma
                    },
                    dataType: "html",
                    encode: "true"
                })
                .done(function(datos) {
                    $("#cuerpoTablaDatos").html(datos);
                    $('#tablaConf tbody tr td:nth-child(2)').each( function(){
                        cant.push( $(this).text() );       
                    });
                    $('#tablaConf tbody tr td:nth-child(3)').each( function(){
                        dias.push( $(this).text() );       
                    });
                    $("#dias").val(dias[0]);
                    $("#ejemplares").val(cant[0]);
                })
                .fail(function(){
                    $("#cuerpoTabla").html("<tr><td colspan='3' class='text-center'><fmt:message key="config.notif"/></td></tr>");
                    alert("Error");
                });
                
                $("#rol").change(function (){
                    $("#dias").val(dias[this.value - 1]);
                    $("#ejemplares").val(cant[this.value - 1]);
                });
            });
        </script>
	</body>
</html>
