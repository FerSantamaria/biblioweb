<%-- 
    Document   : mantoMora
    Created on : 05-04-2018, 10:59:29 AM
    Author     : José Fernando Flores Santamaría - FS150192
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="ajustes" class="sv.edu.udb.beans.Mora" scope="request"/>
<jsp:setProperty name="ajustes" property="costo" param="costo" />

<% 
    String act = request.getParameter("action");
    
    switch(act){
        case "insertar":
            if(ajustes.getEdicion()){
                response.sendRedirect("../configuracion.jsp?done=editarAjustes");
            } else {
                response.sendRedirect("../configuracion.jsp");
            }           
            break;
    }
%>
