<%-- 
    Document   : ingresarUsuario
    Created on : 04-25-2018, 10:55:57 PM
    Author     : José Fernando Flores Santamaría - FS150192
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="infod" class="sv.edu.udb.beans.Documento" scope="session"/>
<jsp:setProperty name="infod" property="id" param="id" />
<jsp:setProperty name="infod" property="categoria" param="categoria" />
<jsp:setProperty name="infod" property="titulo" param="titulo" />
<jsp:setProperty name="infod" property="edicion" param="edicion" />
<jsp:setProperty name="infod" property="autor" param="autor" />
<jsp:setProperty name="infod" property="editorial" param="editorial" />
<jsp:setProperty name="infod" property="descripcion" param="descripcion" />
<jsp:setProperty name="infod" property="resumen" param="resumen" />
<jsp:setProperty name="infod" property="recurso" param="recurso" />
<jsp:setProperty name="infod" property="contenido" param="contenido" />
<jsp:setProperty name="infod" property="temas" param="temas" />
<jsp:setProperty name="infod" property="isbn" param="isbn" />
<jsp:setProperty name="infod" property="ubicacion" param="ubicacion" />
<jsp:setProperty name="infod" property="cantidad" param="cantidad" />
<jsp:setProperty name="infod" property="notas" param="notas" />

<% 
    String act = request.getParameter("action");
    
    switch(act){
        case "insertar":
            infod.getInsertar();
            request.setAttribute("done","insertar");
            response.sendRedirect("../infoDocumento.jsp?done=insertar");
            break;
            
        case "editar":
            infod.getEditar();
            request.setAttribute("done","editar");
            response.sendRedirect("../infoDocumento.jsp?done=editar");
            break;
            
        case "eliminar":
            infod.getEliminar();
            request.setAttribute("done","eliminar");
            if(infod.isError()){
                response.sendRedirect("../documentos.jsp?done=eliminar&error=true");
            } else {
                response.sendRedirect("../documentos.jsp?done=eliminar");
            }
            break;
    }
%>