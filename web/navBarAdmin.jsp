<%-- 
    Document   : navBarAdmin
    Created on : 04-24-2018, 11:56:21 AM
    Author     : José Fernando Flores Santamaría - FS150192
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="menu.jsp">
                <img src="imagenes/logo.png" height="37px" width="105px" class="img-responsive logo" alt="<fmt:message key="titulo.sistema"/>">
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav hidden-sm hidden-md hidden-lg">
                <li><a href="categorias.jsp"><span class="glyphicon glyphicon-th" aria-hidden="true"></span> <fmt:message key="categoria.titulo"/></a></li>
                <li><a href="usuarios.jsp"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <fmt:message key="usuario.titulo"/></a></li>
                <li><a href="documentos.jsp"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> <fmt:message key="documento.titulo"/></a></li>
                <li><a href="prestamos.jsp"><span class="glyphicon glyphicon-sort" aria-hidden="true"></span> <fmt:message key="prestamo.titulo"/></a></li>
                <li><a href="reportes.jsp"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> <fmt:message key="reporte.titulo"/></a></li>
                <li><a href="configuracion.jsp"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> <fmt:message key="config.titulo"/></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="cerrar.jsp"><fmt:message key="navbar.salir"/></a></li>
            </ul>
        </div>
    </div>
</nav>
