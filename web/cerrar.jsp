<%-- 
    Document   : cerrar
    Created on : 05-07-2018, 04:56:58 PM
    Author     : Jos� Fernando Flores Santamar�a - FS150192
--%>

<%@ page session="true" %>
<%
    HttpSession sesionUsuario = request.getSession();
    sesionUsuario.invalidate();
    response.sendRedirect("login.jsp");
%>
