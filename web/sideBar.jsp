<%-- 
    Document   : sideBar
    Created on : 04-24-2018, 03:56:09 PM
    Author     : José Fernando Flores Santamaría - FS150192
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="col-sm-3 col-md-2 sidebar">
    <ul class="nav nav-sidebar">
        <li><a href="estado.jsp"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <fmt:message key="estado.titulo"/></a></li>
        <li><a href="index.jsp"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> <fmt:message key="busqueda.titulo"/></a></li>
        <hr class="separador">
        <li><a href="categorias.jsp"><span class="glyphicon glyphicon-th" aria-hidden="true"></span> <fmt:message key="categoria.titulo"/></a></li>
        <li><a href="usuarios.jsp"><fa class="fa fa-users" aria-hidden="true"></fa> <fmt:message key="usuario.titulo"/></a></li>
        <li><a href="documentos.jsp"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> <fmt:message key="documento.titulo"/></a></li>
        <li><a href="prestamos.jsp"><span class="glyphicon glyphicon-sort" aria-hidden="true"></span> <fmt:message key="prestamo.titulo"/></a></li>
        <li><a href="reportes.jsp"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> <fmt:message key="reporte.titulo"/></a></li>
        <li><a href="configuracion.jsp"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> <fmt:message key="config.titulo"/></a></li>
    </ul>
</div>
