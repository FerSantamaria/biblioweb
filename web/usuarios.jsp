<%-- 
    Document   : usuarios
    Created on : 04-24-2018, 04:03:32 PM
    Author     : José Fernando Flores Santamaría - FS150192
--%>
<%@page session="true"%>
<%
    HttpSession sesionUsuario = request.getSession();
    
    if(sesionUsuario.getAttribute("id") == null){
        response.sendRedirect("login.jsp?error=sesion");
    } else {
        if(!sesionUsuario.getAttribute("tipo").toString().equals("1")){
            response.sendRedirect("estado.jsp");
        }
    }
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="listaUsuarios" class="sv.edu.udb.beans.Usuario" scope="page"/>
<% listaUsuarios.getLista(); %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- <link rel="icon" href="../../favicon.ico"> -->

		<title><fmt:message key="titulo.sistema"/></title>

		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		<link href="css/dashboard.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>

        <jsp:include page="navBarAdmin.jsp" />

		<div class="container-fluid">
			<div class="row">

                <jsp:include page="sideBar.jsp" />

				<div class="col-sm-9 col-md-10 col-md-offset-2 col-sm-offset-3 col-xs-12 main">
					<h1 class="page-header"><fmt:message key="usuario.titulo"/></h1>
					
                    <c:if test="${not empty param.done}">
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>
                                <fmt:message key="usuario.eliminar"/>
                            </strong>
                        </div>
                    </c:if>
                    
					<div class="col-md-12">
						<h3><fmt:message key="usuario.nuevo"/></h3>
						<a href="infoUsuario.jsp">
							<button class="btn btn-primary"><fmt:message key="usuario.nuevo"/></button>
						</a>
					</div>

					<div class="divide-20"></div>

					<div class="col-md-12">
						<h3><fmt:message key="usuario.registrados"/></h3>
                        <div class="table-responsive">
                            <table class="table table-striped table-responsive">
                                <tr>
                                    <th><fmt:message key="usuario.id"/></th>
                                    <th><fmt:message key="usuario.nombre"/></th>
                                    <th><fmt:message key="usuario.tipo"/></th>
                                    <th><fmt:message key="usuario.opciones"/></th>
                                </tr>
                                <c:forEach items="${listaUsuarios.lista}" var="registro">
                                    <tr>
                                        <td>${registro.id}</td>
                                        <td>${registro.nombres}  ${registro.apellidos}</td>
                                        <td>
                                            <span class="label label-success">
                                                <c:choose>
                                                    <c:when test="${registro.tipo == 1}">
                                                        <fmt:message key="tipo.admin"/>
                                                    </c:when>
                                                    <c:when test="${registro.tipo == 2}">
                                                        <fmt:message key="tipo.maestro"/>
                                                    </c:when>
                                                    <c:when test="${registro.tipo == 3}">
                                                        <fmt:message key="tipo.alumno"/>
                                                    </c:when>
                                                </c:choose>
                                            </span>
                                        </td>
                                        <td>
                                            <a class="nuevaContra btn btn-default btn-circle btn-primary" title="<fmt:message key="ayuda.contra"/>" data-id="${registro.id}"><i class="fa fa-key"></i></a>
                                            <a class="btn btn-default btn-circle btn-primary" href="infoUsuario.jsp?action=editar&id=${registro.id}" title="<fmt:message key="ayuda.editar"/>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                                            <a class="btn btn-default btn-circle btn-primary" href="manto/mantoUsuario.jsp?action=eliminar&id=${registro.id}" title="<fmt:message key="ayuda.eliminar"/>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
					</div>

                    <div id="modalContra" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title"><fmt:message key="usuario.cambioContra"/></h4>
                                </div>
                                <div class="modal-body" id="contModal">
                                    <fmt:message key="usuario.id"/>: <span id="idUsuario"></span><br>
                                    <fmt:message key="usuario.contra"/>: <span><strong id="contContra"></strong></span>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>

		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/ie10-viewport-bug-workaround.js"></script>
        <script src="js/util.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".nuevaContra").click(function(event){
                    var idUser = $(this).data("id");
                    
                    $.ajax({
                        url: "NuevaContra",
                        type: "POST",
                        data: { 
                            id : idUser
                        },
                        dataType: "json",
                        encode: "true"
                    })
                    .done(function(datos) {
                        if(!datos.error){
                            $("#idUsuario").html(datos.usuario);
                            $("#contContra").html(datos.contrasena);
                            $("#modalContra").modal("show");
                        } else {
                            alert("Error");
                        }                     
                    })
                    .fail(function(){
                        alert("Error");
                    });
                });        
            });
        </script>            
	</body>
</html>

