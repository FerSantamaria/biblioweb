<%-- 
    Document   : login
    Created on : 04-24-2018, 03:50:54 PM
    Author     : José Fernando Flores Santamaría - FS150192
--%>
<%@page session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:if test="${not empty param.txtUsuario && not empty param.txtContrasena}">
    <jsp:useBean id="inicio" class="sv.edu.udb.beans.Usuario" scope="request"/>
    <jsp:setProperty name="inicio" property="id" param="txtUsuario"/>
    <jsp:setProperty name="inicio" property="contrasena" param="txtContrasena"/>
    <c:set var="exito" value="${inicio.sesion}" />
    
    <c:choose>
        <c:when test="${exito == false}">
            <if test="${inicio != null}">
                <c:remove var="inicio" />
            </if>       
            <%
                response.sendRedirect("login.jsp?error=true");
            %>
        </c:when>
        <c:when test="${exito == true}">
            <%
                HttpSession sesionUsuario = request.getSession();
                inicio.getDatos();
                sesionUsuario.setAttribute("id", inicio.getId()); 
                sesionUsuario.setAttribute("tipo", inicio.getTipo());
                
                switch(inicio.getTipo()){
                    case 1:
                        response.sendRedirect("menu.jsp");
                        break;
                    case 2:
                    case 3:
                        response.sendRedirect("estado.jsp");
                        break;
                }                
            %>
        </c:when>
    </c:choose>
</c:if>

<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- <link rel="icon" href="../../favicon.ico"> -->

		<title><fmt:message key="titulo.sistema"/></title>

		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		<link href="css/dashboard.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>
        
        <jsp:include page="navBar.jsp" />

		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-xs-12 main">
					
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
                            <div class="divide-80"></div>
                            <c:if test="${param.error}">
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>Error</strong> <fmt:message key="login.error1"/>.
                                </div>
                            </c:if>
                            
                            <c:if test="${param.error == 'sesion'}">
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>Error</strong> <fmt:message key="login.error2"/>.
                                </div>
                            </c:if>
                            
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="col-md-12 clearfix">
									<div class="divide-10"></div>
										<center>
											<!-- <img src="imagenes/logo.png" alt="Sistema Bibliotecario" class="img-responsive" width="200px"> -->
											<h3 class="text-center"><fmt:message key="login.titulo"/></h3>
										</center>
									</div>
									<div class="col-md-12">
                                        <form class="form-horizontal" action="login.jsp" method="POST">
											<div class="form-group">
												<label for="txtUsuario" class="col-md-3 col-xs-4 control-label"><fmt:message key="usuario.id"/>:</label>
												<div class="col-md-9 col-xs-8">
													<input type="text" class="form-control" id="txtUsuario" name="txtUsuario">
												</div>
											</div>
											<div class="form-group">
												<label for="txtUsuario" class="col-md-3 col-xs-4 control-label"><fmt:message key="usuario.contra"/>:</label>
												<div class="col-md-9 col-xs-8">
													<input type="password" class="form-control" id="txtContrasena" name="txtContrasena">
												</div>
											</div>
											<div class="form-group">
												<label for="txtUsuario" class="col-md-3 col-xs-4 control-label"></label>
												<div class="col-md-9 col-xs-8">
													<button type="submit" class="btn btn-primary pull-right" id="btnIniciar"><fmt:message key="boton.sesion"/></button>
												</div>
											</div>               
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
						 
				</div>
			</div>
		</div>

		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/ie10-viewport-bug-workaround.js"></script>
	</body>
</html> 
